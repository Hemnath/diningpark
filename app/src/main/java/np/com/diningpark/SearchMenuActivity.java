package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import np.com.diningpark.fragment.OrderListAdapter;
import np.com.diningpark.fragment.SearchItemAdapter;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;

public class SearchMenuActivity extends AppCompatActivity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    ListView searchList, OrderList;
    TextView txtCustName;
    OrderListAdapter orderList;
    SearchItemAdapter searchItemAdapter;
    TextView total, vat, serCharge, payable, strVat, strSerCharge;
    SearchView mSearchView;
    Button back, orderPlace;
    TextView txtView;
    Intent myIntent;
    float serviceChrgRate = 1, vatRate = 1;

    public static final String UserType = "userTypeKey";
    public static final String DeviceId = "DeviceId";
    public static final String ServiceChargeRate = "ServiceChargeRate";
    public static final String VatRate = "VatRate";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    String deviceId, tokenId;
    String tableName, orderMasterId, tableID;
    String type;
    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_menu);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        mydb = new DBHelper(this);

        deviceId = sharedpreferences.getString(DeviceId, null);
        tokenId = sharedpreferences.getString("TokenId", null);
        tableID = String.valueOf(sharedpreferences.getInt("tableId", 1));
        serviceChrgRate = Float.parseFloat(sharedpreferences.getString(ServiceChargeRate, "0"));
        vatRate = Float.parseFloat(sharedpreferences.getString(VatRate, "0"));

        Intent intent = getIntent();

        orderMasterId = intent.getStringExtra("orderMasterId");
        tableName = intent.getStringExtra("tableName");
        tableID = intent.getStringExtra("tableId");

        String custName = "Dining Park";

        txtCustName = (TextView) findViewById(R.id.custName);
        txtCustName.setText(custName + " ( Table No. " + tableName + " )");

        searchList = (ListView) findViewById(R.id.listSearch);
        searchItemAdapter = new SearchItemAdapter(this, tokenId, deviceId, tableName, orderMasterId, tableID);
        searchList.setAdapter(searchItemAdapter);
        searchList.setTextFilterEnabled(true);

        OrderList = (ListView) findViewById(R.id.orderList);
        OrderList.getOverscrollFooter();
        orderList = new OrderListAdapter(this);
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);

        OrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                myIntent = new Intent(SearchMenuActivity.this, EditOrderListActivity.class);
                myIntent.putExtra("parents", "search");
                myIntent.putExtra("orderMasterId", orderMasterId);
                myIntent.putExtra("tableId", tableID);
                startActivity(myIntent);
            }
        });

        mSearchView = (SearchView) findViewById(R.id.searchView);
        mSearchView.setOnQueryTextListener(this);
        mSearchView.setOnCloseListener(this);

        txtView = (TextView) findViewById(R.id.txtView);
        back = (Button) findViewById(R.id.btnBack);
        txtView.setOnClickListener(goBack);
        back.setOnClickListener(goBack);

        total = (TextView) findViewById(R.id.txtTotal);
        vat = (TextView) findViewById(R.id.txtVat);
        serCharge = (TextView) findViewById(R.id.txtSercharge);
        payable = (TextView) findViewById(R.id.txtPayable);
        strVat = (TextView) findViewById(R.id.txtStrVat);
        strSerCharge = (TextView) findViewById(R.id.txtStrSercharge);

        orderPlace = (Button) findViewById(R.id.btnOrderPlace);

        orderPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sharedpreferences.contains(UserType)) {
                    type = sharedpreferences.getString(UserType, null);
                    assert type != null;
                    if (type.equalsIgnoreCase("waiter")) {
                        if (orderMasterId.equals("0") || orderMasterId.equals(""))
                            custNameAndMessage();
                        else
                            orderMessage();
                    } else {
                        orderMessage();
                    }
                    orderList.notifyDataSetChanged();
                }
            }
        });

    }

    View.OnClickListener goBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (sharedpreferences.contains(UserType)) {
                type = sharedpreferences.getString(UserType, null);
                orderList.setOrderList(SearchMenuActivity.this);
                orderList.notifyDataSetChanged();
                setTotalInfo();
                if (type.equalsIgnoreCase("waiter")) {
                    myIntent = new Intent(SearchMenuActivity.this, TableDetailActivity.class);
                    myIntent.putExtra("orderMasterId", orderMasterId);
                    myIntent.putExtra("tableName", tableName);
                    myIntent.putExtra("tableId", tableID);
                    myIntent.putExtra("orderCall", "no");
                } else if (type.equalsIgnoreCase("Table")) {
                    myIntent = new Intent(SearchMenuActivity.this, MainActivity.class);
                    myIntent.putExtra("orderMasterId", orderList.getOrderMasterId());
                    myIntent.putExtra("tableName", tableID);
                    myIntent.putExtra("tableId", tableID);
                }
            }
            startActivity(myIntent);
        }
    };

    public void setOrderMasterId(String MasterId) {
        orderMasterId = MasterId;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        searchItemAdapter.filter(query);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        searchItemAdapter.filter(newText);
        return false;
    }

    @Override
    public boolean onClose() {
        return false;
    }

    public void itemAddInfo(String name) {
        Toast.makeText(this, name + " Added on Order List",
                Toast.LENGTH_SHORT).show();
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);
        orderPlace.setEnabled(true);
        if (mydb.getOrderStatus() >= 5) {
            orderPlace.setEnabled(false);
        }
        setTotalInfo();
    }

    @Override
    public void onBackPressed() {
        if (sharedpreferences.contains(UserType)) {
            type = sharedpreferences.getString(UserType, null);
            if (type.equalsIgnoreCase("waiter")) {
                myIntent = new Intent(SearchMenuActivity.this, TableDetailActivity.class);
                myIntent.putExtra("orderMasterId", orderMasterId);
                myIntent.putExtra("tableName", tableName);
                myIntent.putExtra("tableId", tableID);
                myIntent.putExtra("orderCall", "no");
            } else if (type.equalsIgnoreCase("Table")) {
                myIntent = new Intent(SearchMenuActivity.this, MainActivity.class);
            }
        }
        startActivity(myIntent);
    }

    public void reset() {
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);
        orderList.notifyDataSetChanged();
    }

    public void setTotalInfo() {
        float p, takeP, vt, sc, pay;
        p = mydb.getPriceSum();
        takeP = mydb.getTakeAwayPriceSum();
        sc = (p - takeP) * serviceChrgRate / 100;
        vt = (p + sc) * vatRate / 100;
        pay = p + vt + sc;
        total.setText(String.format("%.0f", p));
        vat.setText(String.format("%.0f", vt));
        serCharge.setText(String.format("%.0f", sc));
        strVat.setText(("Vat " + Float.toString(vatRate) + "% :"));
        strSerCharge.setText(("Services Charge " + Float.toString(serviceChrgRate) + "% :"));
        payable.setText(String.format("%.0f", pay));
    }

    public boolean orderMessage() {
        final String[] strMsg = new String[1];
        final boolean[] done = {false};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.order_message_dialog, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle("Message for Kitchen");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                EditText txtMSG = (EditText) dialogView.findViewById(R.id.edit1);
                txtMSG.setHint("Write Message");
                strMsg[0] = txtMSG.getText().toString();
                orderList.pushNewOrderList1(tokenId, Integer.parseInt(tableID), deviceId, orderMasterId, strMsg[0]);
                orderList.placeOrderList();
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return done[0];
    }


    public boolean custNameAndMessage() {
        final String[] strMsg = new String[4];
        final boolean[] done = {false};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.emp_name_message, null);
        dialogBuilder.setView(dialogView);
        final EditText txtMSG = (EditText) dialogView.findViewById(R.id.edit1);
        txtMSG.setHint("Message");
        final EditText txtName = (EditText) dialogView.findViewById(R.id.edit2);
        txtName.setHint("Name");
        final EditText txtMobile = (EditText) dialogView.findViewById(R.id.edit3);
        txtMobile.setHint("Mobile No.");
        final EditText txtPerson = (EditText) dialogView.findViewById(R.id.edit4);
        txtPerson.setHint("No. of Person");
        dialogBuilder.setTitle("Customer Details");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                strMsg[0] = txtMSG.getText().toString();
                API_Setting.CustomerName = txtName.getText().toString();
                API_Setting.MobileNo = txtMobile.getText().toString();
                API_Setting.NoOfPerson = txtPerson.getText().toString();
                orderList.pushNewOrderList1(tokenId, Integer.parseInt(tableID), deviceId, orderMasterId, strMsg[0]);
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return done[0];
    }

    @Override
    public void onResume() {
        super.onResume();
        reset();
        refresh();
        setTotalInfo();
    }

    public void refresh() {
        if (mydb.haveOrderPlace() && mydb.getOrderStatus() < 5) {
            orderPlace.setText("UPDATE ORDER");
        } else {
            orderPlace.setText("PLACE ORDER");
        }
        if (mydb.haveNewOrder()) {
            orderPlace.setEnabled(true);
        } else {
            orderPlace.setEnabled(false);
        }

        if (mydb.getOrderStatus() >= 5) {
            orderPlace.setEnabled(false);
        }
        reset();
    }

}
