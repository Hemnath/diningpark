package np.com.diningpark;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import np.com.diningpark.fragment.WaiterMsgAdapter;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.JSONParserCustom;

public class WaiterMessageActivity extends AppCompatActivity {

    private ListView waiterMsglistView;
    API_Setting URL;
    String url;
    WaiterMsgAdapter waiterMsgAdapter;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    public static final String DeviceId = "DeviceId";
    public static final String TableNo = "TableNo";

    private String strdeviceId,Token,strTableNo;
    int tableId;
    private ProgressDialog progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiter_massege);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();


        URL = new API_Setting();

        strdeviceId = sharedpreferences.getString(DeviceId, null);
        Token = sharedpreferences.getString("TokenId", null);
        tableId = sharedpreferences.getInt("tableId", 1);
        strTableNo = sharedpreferences.getString(TableNo, null);

        waiterMsglistView = (ListView) findViewById(R.id.noti_bill_listview);
        waiterMsgAdapter=new WaiterMsgAdapter(this);
        waiterMsglistView.setAdapter(waiterMsgAdapter);

        waiterMsglistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
            }
        });
        if (isConnected()) {
            new MSGJSONParse().execute();
        } else {
            showAlertDialog(WaiterMessageActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isConnected()) {
            new MSGJSONParse().execute();
        } else {
            showAlertDialog(WaiterMessageActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }


    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    //todo: Menu Item JSON.

    private class MSGJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.MyMessage ;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            System.out.println("Menu list == "+json);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("List");
                waiterMsgAdapter.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String InvoiceNo = jsonObject.optString("InvoiceNo");
                    String Message = jsonObject.optString("Message");
                    String OfflineMessageId = jsonObject.optString("OfflineMessageId");
                    String OrderMasterId = jsonObject.optString("OrderMasterId");
                    String SentTime = jsonObject.optString("SentTime");
                    String TableNumber = jsonObject.optString("TableNumber");
                    waiterMsgAdapter.addMSG(InvoiceNo,Message,OfflineMessageId,OrderMasterId,SentTime,TableNumber,false);
                    waiterMsgAdapter.notifyDataSetChanged();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
