package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import np.com.diningpark.fragment.slideShowImage;

public class ImageSlideShow extends AppCompatActivity {

    ViewPager viewPager;
    slideShowImage slideShowImage;

    public static final String UserType = "userTypeKey";
    public static final String DeviceId = "DeviceId";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    String deviceId, tokenId;

    Intent myIntent;

    String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slide_show);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sharedpreferences.contains(UserType)) {
                    type = sharedpreferences.getString(UserType, null);
                    if (type.equalsIgnoreCase("waiter")) {
                        myIntent = new Intent(ImageSlideShow.this, np.com.diningpark.WaiterMainActivity.class);

                    } else if (type.equalsIgnoreCase("Table")) {
                        myIntent = new Intent(ImageSlideShow.this, np.com.diningpark.MainActivity.class);
                    }
                }
                startActivity(myIntent);
            }
        });

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        deviceId = sharedpreferences.getString(DeviceId, null);
        tokenId = sharedpreferences.getString("TokenId", null);

        viewPager=(ViewPager)findViewById(R.id.viewPager);
        slideShowImage=new slideShowImage(this,tokenId,deviceId);
        viewPager.setAdapter(slideShowImage);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();


            }
        });
    }


}
