package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import np.com.diningpark.helpers.ServerString;

public class StartActivity extends AppCompatActivity {
    Intent myIntent;
    Button btnWaiter, btnCustomer;
    TextView loading;
    SharedPreferences.Editor edit;
    ServerString serverString;
    String type;

    public static final String MyPREFERENCES = "MyPrefs";
    public static final String UserType = "userTypeKey";
    public static final String TableNo = "TableNo";


    SharedPreferences sharedpreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        btnWaiter = (Button) findViewById(R.id.btnWaiter);
        btnCustomer = (Button) findViewById(R.id.btnCustomer);
        loading = (TextView) findViewById(R.id.textView1);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        edit = sharedpreferences.edit();
        serverString = new ServerString();

        if (sharedpreferences.contains(UserType)) {
            type = sharedpreferences.getString(UserType, null);
            if (type.equalsIgnoreCase("waiter")) {

                myIntent = new Intent(StartActivity.this, WelcomeActivity.class);
                startActivity(myIntent);

            } else if (type.equalsIgnoreCase("Table")) {
                myIntent = new Intent(StartActivity.this, CustomerWelcome.class);
                startActivity(myIntent);
            }
        }

        btnWaiter.setOnClickListener(onClickListener);
        btnCustomer.setOnClickListener(onClickListener);


    }

    @Override
    protected void onResume() {
        super.onResume();
        if (sharedpreferences.contains(UserType)) {
            type = sharedpreferences.getString(UserType, null);
            if (type.equalsIgnoreCase("waiter")) {

                myIntent = new Intent(StartActivity.this, WelcomeActivity.class);
                startActivity(myIntent);

            } else if (type.equalsIgnoreCase("Table")) {
                myIntent = new Intent(StartActivity.this, CustomerWelcome.class);
                startActivity(myIntent);
            }
        }
    }

    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(final View v) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            switch (v.getId()) {
                case R.id.btnWaiter:
                    serverString.setLoginType("Waiter");
                    editor.putString(UserType, "waiter");
                    editor.commit();
                    myIntent = new Intent(StartActivity.this, WelcomeActivity.class);
                    startActivity(myIntent);
                    break;
                case R.id.btnCustomer:
                    showEnterTableDialog();
                    break;

            }

        }

    };

    public void showEnterTableDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.select_table_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);

        dialogBuilder.setTitle("Enter table no. below");
        dialogBuilder.setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String table = edt.getText().toString();
                serverString.setTableNumber(table);
                nextActivity();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void nextActivity() {
        edit.putString(TableNo, serverString.getTableNumber());
        edit.putString(UserType, "Table");
        edit.commit();
        myIntent = new Intent(StartActivity.this, CustomerWelcome.class);
        startActivity(myIntent);
    }


}
