package np.com.diningpark.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by Technomax-Server on 1/14/2016.
 */
public class PagerFragmentAdapter extends FragmentPagerAdapter {

    private static final String ARG_SECTION_NUMBER = "section_number";
    public PagerFragmentAdapter(FragmentManager fm) {
        super(fm);
    }


    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        if (position == 0) {
            fragment = new np.com.diningpark.fragment.OrderListItemFragment();
        }
        if (position == 1) {
            fragment = new np.com.diningpark.fragment.PlaceOrderListFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        // Show 3 total pages.
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "New Order";
            case 1:
                return "Place Order";
        }
        return null;
    }
}