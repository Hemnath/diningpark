package np.com.diningpark.helpers;


/**
 * Created by Hemnath on 11/23/15.
 */

public class API_Setting {
    public String ServerUrl = "http://10.10.92.105:6004/ResApi.svc/";
    public String UserLogin = "UserLogin";
    public String AppSetting = "AppSetting";
    public String MenuTitleList = "MenuTitleList";
    public String MenuListByTitle = "MenuListByTitle?id=";
    public String MenuList = "MenuList";
    public String TableStatus = "TableStatus";
    public String PostOrdere = "PostOrdere";
    public String OrderDetail="OrderDetail?orderId=";
    public String AddNewOrderItem="AddNewOrderItem";
    public String UpdateOrderStatus= "UpdateOrderStatus?orderId=";
    public String DeleteOrderItem= "DeleteOrderItem?id=";
    public String SaveNewMessage= "SaveNewMessage";
    public String GetTableOrder="GetTableOrder";
    public String GetOrder="GetOrder?Id=";
    public String MyMessage="MyMessage";
    public String GetTableStatus="GetTableStatus?TableId=";
    public String HelpingHandsAvailableTable="HelpingHandsAvailableTable";
    public String SetResetHelpingHandStatus="SetResetHelpingHandStatus?tableId=";


    public static String CustomerName = "Diningpark";
    public static String NoOfPerson = "1";
    public static String MobileNo = "";
    public  static boolean  helpingHand=false;
}
