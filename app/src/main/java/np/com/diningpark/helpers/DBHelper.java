package np.com.diningpark.helpers;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "DiningPark.db";
    public static final String TABLE_NAME = "orderList";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_ITEMID = "itemId";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PRICE = "price";
    public static final String COLUMN_QTY = "qty";
    public static final String COLUMN_CALORIES = "calories";
    public static final String COLUMN_ORDERSTATUS = "orderStatus";
    public static final String COLUMN_OREDERDETAILID = "orderDetailId";
    public static final String COLUMN_OrderMasterId = "OrderMasterId";
    public static final String COLUMN_OrderSequence = "orderSequence";
    public static final String COLUMN_OrderMessage = "orderMessage";
    public static final String COLUMN_TakeAway = "takeAway";
    public static final String COLUMN_DeliveryTime = "deliveryTime";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table orderList" +
                        "(id integer primary key,itemId integer, name text, price real, qty integer,calories text, orderDetailId text, orderSequence integer, orderMessage text, deliveryTime integer, takeAway integer, orderStatus integer)"
        );
        db.execSQL(
                "create table orderDetails" +
                        "(id integer primary key,OrderMasterId integer, InvoiceNo text, orderStatus integer )"
        );
        db.execSQL(
                "create table massegeDetails" +
                        "(id integer primary key, massegeId integer, Message text, InvoiceNo text,TableNumber text, OrderMasterId text,isNew integer)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS orderList");
        db.execSQL("DROP TABLE IF EXISTS orderDetails");
        db.execSQL("DROP TABLE IF EXISTS massegeDetails");
        onCreate(db);
    }

    public boolean insertOrderList(int itemId, String name, float price, int qty, String calories, String orderDetailId, int orderSequence, String orderMessage, int deliveryTime, int takeAway, int orderStatus) {
        /*SQLiteDatabase dbRead = this.getReadableDatabase();
        onUpgrade(dbRead, 9, 10);*/
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("itemId", itemId);
        contentValues.put("name", name);
        contentValues.put("price", price);
        contentValues.put("qty", qty);
        contentValues.put("calories", calories);
        contentValues.put("orderStatus", orderStatus);
        contentValues.put("orderDetailId", orderDetailId);
        contentValues.put("orderSequence", orderSequence);
        contentValues.put("orderMessage", orderMessage);
        contentValues.put("deliveryTime", deliveryTime);
        contentValues.put("takeAway", takeAway);
        db.insert("orderList", null, contentValues);
        return true;

    }

    public boolean insertOldOrderList(int itemId, String name, float price, int qty, String calories, String orderDetailId, int orderSequence, String orderMessage, int deliveryTime,int takeAway, int orderStatus) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        Boolean have = false;
        int orderItemId, ID = 2;
        int oldQty, orderPlaceStatus;
        Cursor res = dbRead.rawQuery("select * from orderList", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            String orderPlaceId = res.getString(res.getColumnIndex(COLUMN_OREDERDETAILID));
            if (orderPlaceId.equalsIgnoreCase(orderDetailId)) {
                have = true;
                oldQty = res.getInt(res.getColumnIndex(COLUMN_QTY));
                ID = res.getInt(res.getColumnIndex(COLUMN_ID));
                qty = qty + oldQty;
            }
            res.moveToNext();
        }
        res.close();
        if (have) {
            return updateOrderList(ID, name, price, qty, calories, orderDetailId, orderSequence, orderMessage, deliveryTime, takeAway, orderStatus);
        } else {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put("itemId", itemId);
            contentValues.put("name", name);
            contentValues.put("price", price);
            contentValues.put("qty", qty);
            contentValues.put("calories", calories);
            contentValues.put("orderStatus", orderStatus);
            contentValues.put("orderDetailId", orderDetailId);
            contentValues.put("orderSequence", orderSequence);
            contentValues.put("orderMessage", orderMessage);
            contentValues.put("deliveryTime", deliveryTime);
            contentValues.put("takeAway", takeAway);
            db.insert("orderList", null, contentValues);
            return true;
        }
    }

    public int getMaxOrderSequence() {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        int orderSequence = 1;
        Cursor res = dbRead.rawQuery("SELECT MAX(orderSequence) as os FROM orderList", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            orderSequence = res.getInt(res.getColumnIndex("os"));
            res.moveToNext();
        }
        res.close();
        return orderSequence;
    }



    public int getMaxOldOrderSequence() {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        int orderSequence = 1;
        Cursor res = dbRead.rawQuery("SELECT MAX(orderSequence) as os FROM orderList WHERE orderStatus > 0", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            orderSequence = res.getInt(res.getColumnIndex("os"));
            res.moveToNext();
        }
        if (orderSequence == 0)
            orderSequence = 1;
        else
            orderSequence++;

        res.close();
        return orderSequence;
    }


    public int getCount(int OrderSequence) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        int count = 0;
        Cursor res = dbRead.rawQuery("SELECT COUNT(*) as os  FROM orderList", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            count = res.getInt(res.getColumnIndex("os"));
            res.moveToNext();
        }
        res.close();
        return count;
    }


    public int getIsOldOrder(String orderDetailId) {
        SQLiteDatabase dbRead = this.getReadableDatabase();
        String orderItemId = "";
        int orderPlaceStatus = 0;
        Cursor res = dbRead.rawQuery("select * from orderList", null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            orderItemId = res.getString(res.getColumnIndex(COLUMN_OREDERDETAILID));
            if (orderItemId.equalsIgnoreCase(orderDetailId)) {
                orderPlaceStatus = res.getInt(res.getColumnIndex(COLUMN_ORDERSTATUS));
            }
            res.moveToNext();
        }
        res.close();
        return orderPlaceStatus;
    }

    public boolean updateOrderList(int Id, String name, float price, int qty, String calories, String orderDetailId, int orderSequence, String orderMessage, int deliveryTime, int takeAway, int orderStatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("name", name);
        contentValues.put("price", price);
        contentValues.put("qty", qty);
        contentValues.put("calories", calories);
        contentValues.put("orderStatus", orderStatus);
        contentValues.put("orderDetailId", orderDetailId);
        contentValues.put("orderSequence", orderSequence);
        contentValues.put("orderMessage", orderMessage);
        contentValues.put("deliveryTime", deliveryTime);
        contentValues.put("takeAway", takeAway);
        db.update("orderList", contentValues, "id = ? ", new String[]{Integer.toString(Id)});
        return true;
    }


    public boolean insertOrderDetails(String OrderMasterId, String InvoiceNo, int orderStatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("OrderMasterId", OrderMasterId);
        contentValues.put("InvoiceNo", InvoiceNo);
        contentValues.put("orderStatus", orderStatus);
        db.insert("orderDetails", null, contentValues);
        return true;
    }

    public boolean insertMassegeDetails(String massegeId, String message, String invoiceNo, String TableNumber, String OrderMasterId, int isNew) {
        //SQLiteDatabase dbRead = this.getReadableDatabase();
        //onUpgrade(dbRead,10,11);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("massegeId", massegeId);
        contentValues.put("Message", message);
        contentValues.put("InvoiceNo", invoiceNo);
        contentValues.put("TableNumber", TableNumber);
        contentValues.put("OrderMasterId", OrderMasterId);
        contentValues.put("isNew", isNew);
        db.insert("massegeDetails", null, contentValues);
        return true;
    }

    public boolean updateMassegeDetails(String massegeId, int isNew) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("massegeId", massegeId);
        contentValues.put("isNew", isNew);

        db.update("MassegeDetails", contentValues, "massegeId = ? ", new String[]{massegeId});
        return true;
    }

    public boolean isNewMassegeId(String id) {
        boolean have = true;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from massegeDetails ORDER BY id DESC", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            if (res.getString(1).equalsIgnoreCase(id))
                have = false;
            res.moveToNext();
        }
        res.close();
        return have;
    }

    public int getIsNew(String MassegeId) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from massegeDetails where massegeId=" + MassegeId + "", null);
        res.moveToFirst();
        int var = res.getInt(6);
        res.close();
        return var;
    }

    public String getOrderMasterId() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderDetails ORDER BY id DESC", null);
        res.moveToFirst();
        String var = res.getString(1);
        res.close();
        return var;
    }

    public Integer getOrderMasterStatus() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderDetails ORDER BY id DESC", null);
        res.moveToFirst();
        int var = res.getInt(3);
        res.close();
        return var;
    }

    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where id=" + id + "", null);
        return res;
    }

    public float getTakeAwayPriceSum() {
      /*  SQLiteDatabase dbRead = this.getReadableDatabase();
        onUpgrade(dbRead,10,11);*/
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select sum(price*qty) from orderList where orderStatus <= 5 AND takeAway = 1 ", null);
        res.moveToFirst();
        Float var = res.getFloat(0);
        res.close();
        return var;
    }

    public float getPriceSum() {
        /*SQLiteDatabase dbRead = this.getReadableDatabase();
        onUpgrade(dbRead,10,11);*/
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select sum(price*qty) from orderList where orderStatus <= 5 ", null);
        res.moveToFirst();
        Float var = res.getFloat(0);
        res.close();
        return var;
    }

    public boolean haveOrderPlace() {
        boolean have;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where orderStatus > 0", null);
        if (res.moveToFirst())
            have = true;
        else
            have = false;
        return have;
    }

    public boolean haveNewOrder() {
        boolean have;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where orderStatus = 0", null);
        if (res.moveToFirst())
            have = true;
        else
            have = false;
        return have;
    }

    public int getOrderStatus() {
        int status;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select orderStatus from orderDetails where orderStatus >= 5 ORDER BY id DESC", null);
        if (res.moveToFirst())
            status = res.getInt(0);
        else
            status = 0;
        return status;
    }


    public int numberOfRows() {
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, TABLE_NAME);
        return numRows;
    }


    public boolean updateOrderDetails(String OrderMasterId, int orderStatus) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("OrderMasterId", OrderMasterId);
        contentValues.put("orderStatus", orderStatus);

        db.update("orderDetails", contentValues, "OrderMasterId = ? ", new String[]{OrderMasterId});
        return true;
    }

    public Integer deleteOrderList(Integer id) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("orderList",
                "id = ? ",
                new String[]{Integer.toString(id)});
    }

    public void deleteAllOrderList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("orderList", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public void deleteAllOldOrderList() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList Where  orderStatus > 0", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            if (!(res.getInt(res.getColumnIndex(DBHelper.COLUMN_ORDERSTATUS)) == 6)) {
                db.delete("orderList", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            }
            res.moveToNext();
        }
        res.close();
    }

    public void deleteAllOrderDetails() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderDetails", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("orderDetails", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public void deleteAllMassegeDetails() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from massegeDetails", null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            db.delete("orderDetails", "id = ? ", new String[]{res.getString(res.getColumnIndex(DBHelper.COLUMN_ID))});
            res.moveToNext();
        }
        res.close();
    }

    public ArrayList<Integer> getAllOrderList1(int OrderSequence) {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where orderSequence = " + OrderSequence, null);
        res.moveToFirst();
        while (res.isAfterLast() == false) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }

    public int geDeliveryTime(int OrderSequence) {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where orderSequence = " + OrderSequence, null);
        int dt = 0;
        res.moveToFirst();
        while (!res.isAfterLast()) {
            dt = res.getInt(res.getColumnIndex(COLUMN_DeliveryTime));
            res.moveToNext();
        }
        res.close();
        return dt;
    }

    public int setDeliveryTime(int OrderSequence, int DelTime) {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList where orderSequence = " + OrderSequence, null);
        res.moveToFirst();
        while (!res.isAfterLast()) {
            updateOrderList(res.getInt(res.getColumnIndex(COLUMN_ID)), res.getString(res.getColumnIndex(COLUMN_NAME)), res.getFloat(res.getColumnIndex(COLUMN_PRICE)), res.getInt(res.getColumnIndex(COLUMN_QTY)), res.getString(res.getColumnIndex(COLUMN_CALORIES)), res.getString(res.getColumnIndex(COLUMN_OREDERDETAILID)), res.getInt(res.getColumnIndex(COLUMN_OrderSequence)), res.getString(res.getColumnIndex(COLUMN_OrderMessage)), DelTime,res.getInt(res.getColumnIndex(COLUMN_TakeAway)), res.getInt(res.getColumnIndex(COLUMN_ORDERSTATUS)));
            res.moveToNext();
        }
        res.close();
        return 1;
    }


    public ArrayList<Integer> getAllOrderList() {
        ArrayList<Integer> array_list = new ArrayList<Integer>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from orderList", null);
        res.moveToFirst();

        while (!res.isAfterLast()) {
            array_list.add(res.getInt(res.getColumnIndex(COLUMN_ID)));
            res.moveToNext();
        }
        res.close();
        return array_list;
    }
}