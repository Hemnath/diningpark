package np.com.diningpark.helpers;

public class FileCache {

	private java.io.File cacheDir;

	public FileCache(android.content.Context context) {
		// Find the dir to save cached images
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			cacheDir = new java.io.File(
					android.os.Environment.getExternalStorageDirectory(),
					"JsonParseTutorialCache");
		else
			cacheDir = context.getCacheDir();
		if (!cacheDir.exists())
			cacheDir.mkdirs();
	}

	public java.io.File getFile(String url) {
		String filename = String.valueOf(url.hashCode());
		// String filename = URLEncoder.encode(url);
		java.io.File f = new java.io.File(cacheDir, filename);
		return f;

	}

	public void clear() {
		java.io.File[] files = cacheDir.listFiles();
		if (files == null)
			return;
		for (java.io.File f : files)
			f.delete();
	}

}