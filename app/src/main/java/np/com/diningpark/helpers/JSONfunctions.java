package np.com.diningpark.helpers;

public class JSONfunctions {

	public static org.json.JSONObject getJSONfromURL(String url) {
		java.io.InputStream is = null;
		String result = "";
		org.json.JSONObject jArray = null;

		// Download JSON data from URL
		try {
			org.apache.http.client.HttpClient httpclient = new org.apache.http.impl.client.DefaultHttpClient();
			org.apache.http.client.methods.HttpPost httppost = new org.apache.http.client.methods.HttpPost(url);
			org.apache.http.HttpResponse response = httpclient.execute(httppost);
			org.apache.http.HttpEntity entity = response.getEntity();
			is = entity.getContent();

		} catch (Exception e) {
			android.util.Log.e("log_tag", "Error in http connection " + e.toString());
		}

		// Convert response to string
		try {
			java.io.BufferedReader reader = new java.io.BufferedReader(new java.io.InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			result = sb.toString();
		} catch (Exception e) {
			android.util.Log.e("log_tag", "Error converting result " + e.toString());
		}

		try {

			jArray = new org.json.JSONObject(result);
		} catch (org.json.JSONException e) {
			android.util.Log.e("log_tag", "Error parsing data " + e.toString());
		}

		return jArray;
	}
}
