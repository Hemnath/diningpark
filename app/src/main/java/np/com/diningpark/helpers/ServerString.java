package np.com.diningpark.helpers;

/**
 * Created by dev on 11/1/15.
 */
public class ServerString {
    private String username;
    private String password;
    private String tableNumber;
    private String DeviceId;
    private String LoginType;

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId;
    }

    public String getQty() {
        return Qty;
    }

    public void setQty(String qty) {
        Qty = qty;
    }

    private  String menuId;
    private  String Qty;

    public String getPassword() {
        //System.out.println("password in getter: " + password);
        return password;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public String getUsername() {
        //System.out.println("username in getter: " + username);
        return username;
    }

    public void setUsername(String username) {

        this.username = username;
    }
    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getDeviceId() {
        return DeviceId;
    }

    public void setDeviceId(String setDeviceId) {
        this.DeviceId = setDeviceId;
    }

    public String getLoginType() {
        return LoginType;
    }

    public void setLoginType(String setLoginType) {
        this.LoginType = setLoginType;
    }
}
