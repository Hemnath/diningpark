package np.com.diningpark.helpers;

import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import np.com.diningpark.MainActivity;
import np.com.diningpark.R;
import np.com.diningpark.TableDetailActivity;
import np.com.diningpark.WaiterMainActivity;
import np.com.diningpark.WelcomeActivity;

public class AdoutusActivity extends AppCompatActivity {
    private WebView mWebView;
    Intent myIntent;

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    public static final String UserType = "userTypeKey";
    String type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adoutus);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                if (sharedpreferences.contains(UserType)) {
                    type = sharedpreferences.getString(UserType, null);
                    if (type.equalsIgnoreCase("waiter")) {
                        myIntent = new Intent(AdoutusActivity.this, np.com.diningpark.WaiterMainActivity.class);

                    } else if (type.equalsIgnoreCase("Table")) {
                        myIntent = new Intent(AdoutusActivity.this, np.com.diningpark.MainActivity.class);
                    }
                }
                startActivity(myIntent);
            }
        });

        mWebView = (WebView) findViewById(R.id.webView);
        mWebView.setWebViewClient(new MyBrowser());
        String url = "http://192.168.0.251:8083/Page/name/About-us";

        mWebView.getSettings().setLoadsImagesAutomatically(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        mWebView.loadUrl(url);

    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return;
        }

        // Otherwise defer to system default behavior.
        super.onBackPressed();
    }
}
