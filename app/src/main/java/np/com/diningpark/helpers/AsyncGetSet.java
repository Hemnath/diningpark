package np.com.diningpark.helpers;

/**
 * Created by dev on 11/2/15.
 */
public class AsyncGetSet {
     String userId;
     String token;

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        System.out.println("userid in getter: " + userId );
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token)
    {
        System.out.println("token in getter: " + token);
        this.token = token;
    }
}
