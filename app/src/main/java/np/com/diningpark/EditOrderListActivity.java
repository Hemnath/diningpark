package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import np.com.diningpark.fragment.EditOrderListAdapter;
import np.com.diningpark.helpers.DBHelper;

public class EditOrderListActivity extends AppCompatActivity {
    private ListView EditOrderList;
    private EditOrderListAdapter editOrderListAdapter;
    Button cancel, done;
    Intent myIntent;
    private String parents;
    private String orderMasterId;
    String tableName, tableID;
    private DBHelper mydb;


    static final int DELTA = 300;
    float historicX = Float.NaN, historicY = Float.NaN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_order_list);

        Intent intent = getIntent();
        parents = intent.getStringExtra("parents");
        orderMasterId = intent.getStringExtra("orderMasterId");
        tableName = intent.getStringExtra("tableName");
        tableID = intent.getStringExtra("tableId");

        mydb = new DBHelper(this);

        EditOrderList = (ListView) findViewById(R.id.editorderList);
        editOrderListAdapter = new EditOrderListAdapter(this);
        EditOrderList.setAdapter(editOrderListAdapter);
        editOrderListAdapter.setOrderList(this);

        cancel = (Button) findViewById(R.id.editCancel);
        done = (Button) findViewById(R.id.editDone);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (parents.equalsIgnoreCase("main"))
                    myIntent = new Intent(EditOrderListActivity.this, MainActivity.class);
                else if (parents.equalsIgnoreCase("search")) {
                    myIntent = new Intent(EditOrderListActivity.this, SearchMenuActivity.class);
                    myIntent.putExtra("orderMasterId", orderMasterId);
                    myIntent.putExtra("tableName", tableName);
                    myIntent.putExtra("tableId", tableID);
                } else if (parents.equalsIgnoreCase("waiter")) {
                    myIntent = new Intent(EditOrderListActivity.this, TableDetailActivity.class);
                    myIntent.putExtra("orderMasterId", orderMasterId);
                    myIntent.putExtra("tableName", tableName);
                    myIntent.putExtra("tableId", tableID);
                    myIntent.putExtra("orderCall", "no");
                }
                myIntent.putExtra("orderMasterId", orderMasterId);
                startActivity(myIntent);
                EditOrderListActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editOrderListAdapter.updateOrderList();
                if (parents.equalsIgnoreCase("main"))
                    myIntent = new Intent(EditOrderListActivity.this, MainActivity.class);
                else if (parents.equalsIgnoreCase("search")) {
                    myIntent = new Intent(EditOrderListActivity.this, SearchMenuActivity.class);
                    myIntent.putExtra("orderMasterId", orderMasterId);
                    myIntent.putExtra("tableName", tableName);
                    myIntent.putExtra("tableId", tableID);
                } else if (parents.equalsIgnoreCase("waiter")) {
                    myIntent = new Intent(EditOrderListActivity.this, TableDetailActivity.class);
                    myIntent.putExtra("orderMasterId", orderMasterId);
                    myIntent.putExtra("tableName", tableName);
                    myIntent.putExtra("tableId", tableID);
                    myIntent.putExtra("orderCall", "no");
                }
                myIntent.putExtra("orderMasterId", orderMasterId);
                startActivity(myIntent);
                EditOrderListActivity.this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        EditOrderList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        historicX = event.getX();
                        historicY = event.getY();
                        return false;

                    case MotionEvent.ACTION_UP:
                        if (EditOrderList.getChildAt(0) != null) {
                            int heightOfEachItem = EditOrderList.getChildAt(0).getHeight();
                            int heightOfFirstItem = -EditOrderList.getChildAt(0).getTop() + EditOrderList.getFirstVisiblePosition() * heightOfEachItem;
                            final int firstPosition = (int) Math.ceil(heightOfFirstItem / heightOfEachItem);

                            final int wantedPosition = (int) Math.floor((historicY - EditOrderList.getChildAt(0).getTop()) / heightOfEachItem) + firstPosition;

                            final int wantedChild = wantedPosition - firstPosition;

                            if (event.getX() - historicX < -DELTA) {
                                if (wantedChild < 0 || wantedChild >= EditOrderList.getCount() || wantedChild >= EditOrderList.getChildCount()) {
                                    return true;
                                }
                                EditOrderList.getChildAt(wantedChild).startAnimation(outToLeftAnimation(500));
                                new java.util.Timer().schedule(
                                        new java.util.TimerTask() {
                                            @Override
                                            public void run() {
                                                // editOrderListAdapter.addhiddenPositions(wantedPosition);
                                            }
                                        },
                                        500
                                );
                                editOrderListAdapter.notifyDataSetChanged();
                                return true;

                            } else if (event.getX() - historicX > DELTA) {
                                if (wantedChild < 0 || wantedChild >= EditOrderList.getCount() || wantedChild >= EditOrderList.getChildCount()) {

                                    return true;
                                }
                                EditOrderList.getChildAt(wantedChild).startAnimation(outToRightAnimation(500));
                                new java.util.Timer().schedule(
                                        new java.util.TimerTask() {
                                            @Override
                                            public void run() {
                                                //editOrderListAdapter.addhiddenPositions(wantedPosition);
                                            }
                                        },
                                        500
                                );
                                return true;
                            }

                        }
                        return true;
                    default:
                        return false;
                }
            }
        });


    }

    private Animation outToLeftAnimation(int duration) {
        Animation outtoLeft = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, -1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoLeft.setDuration(duration);
        outtoLeft.setInterpolator(new AccelerateInterpolator());
        return outtoLeft;
    }

    private Animation outToRightAnimation(int duration) {
        Animation outtoRight = new TranslateAnimation(
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, +1.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f,
                Animation.RELATIVE_TO_PARENT, 0.0f);
        outtoRight.setDuration(duration);
        outtoRight.setInterpolator(new AccelerateInterpolator());
        return outtoRight;
    }

    public boolean orderSequence(final int id, final String name, final Float rate, final Integer qty, final String calories, final String orderDetailId, final int sq, final String orderMessage, final int deliveryTime,final int takeAway, final int orderStatus) {
        final boolean[] done = {false};
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.order_sequence, null);
        dialogBuilder.setView(dialogView);

        final TextView txtDiagSeqNo = (TextView) dialogView.findViewById(R.id.diagSeqNo);
        txtDiagSeqNo.setText(String.valueOf(sq));

        Button btnDecrsSeq = (Button) dialogView.findViewById(R.id.btnDecrsSeq);
        Button btnIncrsSeq = (Button) dialogView.findViewById(R.id.btnIncrsSeq);

        btnIncrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });
        btnDecrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });

        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                mydb.updateOrderList(id, name, rate, qty, calories, orderDetailId, intSeq, orderMessage, mydb.geDeliveryTime(intSeq),takeAway, orderStatus);
                editOrderListAdapter = new EditOrderListAdapter(EditOrderListActivity.this);
                EditOrderList.setAdapter(editOrderListAdapter);
                editOrderListAdapter.setOrderList(EditOrderListActivity.this);
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        return done[0];
    }

}
