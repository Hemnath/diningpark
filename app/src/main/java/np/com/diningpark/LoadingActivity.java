package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class LoadingActivity extends AppCompatActivity {
    Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(7000);
                        myIntent = new Intent(LoadingActivity.this, WelcomeActivity.class);
                        startActivity(myIntent);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
    }
    protected void onResume() {
        super.onResume();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(7000);
                    myIntent = new Intent(LoadingActivity.this, WelcomeActivity.class);
                    startActivity(myIntent);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
