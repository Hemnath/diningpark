package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Point;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import np.com.diningpark.fragment.EditOrderListAdapter;
import np.com.diningpark.fragment.GroupItemImageAdapter;
import np.com.diningpark.fragment.ImageAdapter;
import np.com.diningpark.fragment.LinearListView;
import np.com.diningpark.fragment.OrderListAdapter;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    GridView menuGridview;
    LinearListView menuCategoryList;
    ListView OrderList;
    String name, price, calories, url, strQty, strSeqNum, menuIngredient, menuSuggestion;
    String custName;
    int Qty, item_id, seqNum;
    Intent myIntent;
    NavigationView navigationView;
    ImageAdapter itemListImage;
    GroupItemImageAdapter menuCateAdapter;
    OrderListAdapter orderList;
    TextView total, vat, serCharge, strVat, strSerCharge, payable, txtCustName, billRequestSts;
    int menuCategoryId;
    ProgressDialog progressBar;
    Button increase, decrease, increaseSeq, decreaseSeq, dismissButton, dismissOk, placeOrder, newOrder, bill;
    TextView diagQty, diagSeqNo;


    private Timer autoUpdate;

    public static final String DeviceId = "DeviceId";
    public static final String TableNo = "TableNo";
    public static final String ServiceChargeRate = "ServiceChargeRate";
    public static final String VatRate = "VatRate";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;
    DrawerLayout drawer;

    String strdeviceId = "";
    String Token = "", strTableNo, cMsg;
    int tableId = 0;
    float serviceChrgRate, vatRate;
    DBHelper mydb;
    Dialog dialog;
    API_Setting URL;
    boolean passwordResult = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.popup, null);
                dialogBuilder.setView(dialogView);

                dialogBuilder.setTitle("Message for Waiter");
                dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                    }

                }).setPositiveButton("Call Waiter", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        EditText txtMsg = (EditText) dialogView.findViewById(R.id.CustMsg);
                        cMsg = txtMsg.getText().toString();
                        if (isConnected()) {
                            new SENDMASSAGEJSONParse().execute();
                        } else {
                            showAlertDialog(MainActivity.this, "No Internet Connection",
                                    "You don't have internet connection.", false);
                        }

                    }
                }).setNegativeButton("Message              ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        EditText txtMsg = (EditText) dialogView.findViewById(R.id.CustMsg);
                        cMsg = txtMsg.getText().toString();
                        if (isConnected()) {
                            new SENDMASSAGEJSONParse().execute();
                        } else {
                            showAlertDialog(MainActivity.this, "No Internet Connection",
                                    "You don't have internet connection.", false);
                        }
                    }
                });
                AlertDialog b = dialogBuilder.create();
                b.show();
            }
        });


        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();


        URL = new API_Setting();

        strdeviceId = sharedpreferences.getString(DeviceId, null);
        Token = sharedpreferences.getString("TokenId", null);
        tableId = sharedpreferences.getInt("tableId", 1);
        strTableNo = sharedpreferences.getString(TableNo, null);

        serviceChrgRate = Float.parseFloat(sharedpreferences.getString(ServiceChargeRate, "0"));
        vatRate = Float.parseFloat(sharedpreferences.getString(VatRate, "0"));

        mydb = new DBHelper(this);
        //For Toolbar Spinner
        //setCuisine();

        custName = sharedpreferences.getString("Name", "Dining Park");

        txtCustName = (TextView) findViewById(R.id.custName);
        txtCustName.setText(custName + " ( Table No. " + strTableNo + " )");

        total = (TextView) findViewById(R.id.txtTotal);
        vat = (TextView) findViewById(R.id.txtVat);
        serCharge = (TextView) findViewById(R.id.txtSercharge);
        payable = (TextView) findViewById(R.id.txtPayable);
        strVat = (TextView) findViewById(R.id.txtStrVat);
        strSerCharge = (TextView) findViewById(R.id.txtStrSercharge);

        //Dialog interface
        dialog = new Dialog(MainActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_dailog_layout);

        dismissButton = (Button) dialog.findViewById(R.id.button);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dismissOk = (Button) dialog.findViewById(R.id.add);

        increase = (Button) dialog.findViewById(R.id.btnIncrs);
        decrease = (Button) dialog.findViewById(R.id.btnDecrs);
        increaseSeq = (Button) dialog.findViewById(R.id.btnIncrsSeq);
        decreaseSeq = (Button) dialog.findViewById(R.id.btnDecrsSeq);
        diagQty = (TextView) dialog.findViewById(R.id.diagQty);
        diagSeqNo = (TextView) dialog.findViewById(R.id.diagSeqNo);

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempQty = diagQty.getText().toString();
                int intQty = Integer.parseInt(tempQty);
                intQty++;
                diagQty.setText(Integer.toString(intQty));
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempQty = diagQty.getText().toString();
                int intQty = Integer.parseInt(tempQty);
                if (intQty > 1)
                    intQty--;
                diagQty.setText(Integer.toString(intQty));
            }
        });
        increaseSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = diagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                diagSeqNo.setText(String.valueOf(intSeq));
            }
        });
        decreaseSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = diagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                diagSeqNo.setText(String.valueOf(intSeq));
            }
        });

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        menuGridview = (GridView) findViewById(R.id.itemGridView);
        menuCategoryList = (LinearListView) findViewById(R.id.horizontal_list);
        OrderList = (ListView) findViewById(R.id.orderList);

        menuCategoryList.setDividerThickness(getResources().getDimensionPixelSize(R.dimen.padding_small));

        menuCateAdapter = new GroupItemImageAdapter(this);
        menuCategoryList.setAdapter(menuCateAdapter);
        menuCategoryList.setOnItemClickListener(mListener);

        if (isConnected()) {
            new menuCateJSONParse().execute();
        } else {
            showAlertDialog(MainActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }

        itemListImage = new ImageAdapter(this);
        menuGridview.setAdapter(itemListImage);

        orderList = new OrderListAdapter(this);
        OrderList.setAdapter(orderList);

        OrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                myIntent = new Intent(MainActivity.this, EditOrderListActivity.class);
                myIntent.putExtra("parents", "main");
                myIntent.putExtra("orderMasterId", "");
                startActivity(myIntent);
            }
        });

        menuGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                item_id = itemListImage.getMenuId(position);
                String imageURL = String.valueOf(itemListImage.getImageURL(position));
                name = itemListImage.getItemName(position);
                price = itemListImage.getItemPrice(position);
                calories = itemListImage.getItemCalories(position);
                menuIngredient = itemListImage.getItemmenuIngredient(position);
                menuSuggestion = itemListImage.getItemmenuSuggestion(position);

                if (menuSuggestion.isEmpty() || menuSuggestion.equalsIgnoreCase("null")) {
                    menuSuggestion = "";
                }

                TextView txtdiaPrice = (TextView) dialog.findViewById(R.id.itemdPrice);
                txtdiaPrice.setText(price);
                TextView txtdiaCalories = (TextView) dialog.findViewById(R.id.itemDaiCalories);
                txtdiaCalories.setText(calories);

                TextView txtdiaName = (TextView) dialog.findViewById(R.id.itemdName);
                txtdiaName.setText(name);
                TextView txtMenuIngredient = (TextView) dialog.findViewById(R.id.productDesc);
                txtMenuIngredient.setText(menuIngredient);
                TextView txtMenuSuggestion = (TextView) dialog.findViewById(R.id.txtcalories);
                txtMenuSuggestion.setText(menuSuggestion);
                TextView diagQty = (TextView) dialog.findViewById(R.id.diagQty);
                diagQty.setText("1");
                diagSeqNo.setText(String.valueOf(mydb.getMaxOldOrderSequence()));
                ImageView diaImage = (ImageView) dialog.findViewById(R.id.picture);
                com.squareup.picasso.Picasso.with(MainActivity.this).load(imageURL).into(diaImage);
                dialog.show();

            }
        });

        dismissOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strQty = diagQty.getText().toString();
                strSeqNum = diagSeqNo.getText().toString();
                Qty = Integer.parseInt(strQty);
                seqNum = Integer.parseInt(strSeqNum);
                if (mydb.getOrderStatus() < 5) {
                    addOrderList();
                }
                refresh();
                orderList.notifyDataSetChanged();
                dialog.dismiss();

            }
        });


        placeOrder = (Button) findViewById(R.id.btnOrderPlace);
        newOrder = (Button) findViewById(R.id.btnNewOrder);
        bill = (Button) findViewById(R.id.btnBill);

        billRequestSts = (TextView) findViewById(R.id.billRequestSts);

        placeOrder.setOnClickListener(orderAction);
        newOrder.setOnClickListener(orderAction);
        bill.setOnClickListener(orderAction);


    }

    LinearListView.OnItemClickListener mListener = new LinearListView.OnItemClickListener() {

        @Override
        public void onItemClick(LinearListView parent, View view, int position, long id) {
            menuCategoryId = (int) menuCateAdapter.getItemId(position);
            if (isConnected()) {
                progressBar.show();
                new menuJSONParse().execute();
            }
        }
    };

    public void addOrderList() {
        int intCalories = Integer.parseInt(calories) * Qty;
        float fp = Float.parseFloat(price);
        mydb.insertOrderList(item_id, name, fp, Qty, Integer.toString(intCalories), "", seqNum, "", mydb.geDeliveryTime(seqNum), 0, 0);
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);
        setTotalInfo();
        setTotalInfo();
        Toast.makeText(MainActivity.this, name + " Added on Order List",
                Toast.LENGTH_SHORT).show();
    }

    public void setTotalInfo() {
        float p, takeP, vt, sc, pay;
        p = mydb.getPriceSum();
        takeP = mydb.getTakeAwayPriceSum();
        sc = (p - takeP) * serviceChrgRate / 100;
        vt = (p + sc) * vatRate / 100;
        pay = p + vt + sc;

        total.setText(String.format("%.0f", p));
        vat.setText(String.format("%.0f", vt));
        serCharge.setText(String.format("%.0f", sc));
        strVat.setText(("Vat " + Float.toString(vatRate) + "% :"));
        strSerCharge.setText(("Services Charge " + Float.toString(serviceChrgRate) + "% :"));
        payable.setText(String.format("%.0f", pay));
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            LayoutInflater inflater = this.getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.back_warning_dailog, null);
            dialogBuilder.setView(dialogView);

            final TextView edt = (TextView) dialogView.findViewById(R.id.edit1);

            edt.setText("This will clear all of your new selected food items!!!");
            dialogBuilder.setTitle("Are you sure you want to exit?");

            dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                }

            }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    if (autoUpdate != null) {
                        autoUpdate.cancel();
                        autoUpdate.purge();
                        autoUpdate = null;
                        finish();
                    }
                    mydb.deleteAllOrderList();
                    mydb.deleteAllOrderDetails();
                    myIntent = new Intent(MainActivity.this, TableNoActivity.class);
                    startActivity(myIntent);
                }
            });
            AlertDialog b = dialogBuilder.create();
            b.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        orderList.GetTableOrder(Token, tableId, strdeviceId);
                        orderList.setOrderList(MainActivity.this);
                        orderList.notifyDataSetChanged();
                        setTotalInfo();
                    }
                });
            }
        }, 0, 10000);
        refresh();
    }

    public void refresh() {
        if (mydb.haveOrderPlace() && mydb.getOrderStatus() < 5) {
            newOrder.setEnabled(false);
            bill.setEnabled(true);
            placeOrder.setText("UPDATE ORDER");
        } else {
            placeOrder.setText("PLACE ORDER");
            newOrder.setEnabled(true);
        }
        if (mydb.haveNewOrder()) {
            placeOrder.setEnabled(true);
            bill.setEnabled(false);
        } else {
            placeOrder.setEnabled(false);
        }

        if (mydb.getOrderStatus() >= 5) {
            placeOrder.setEnabled(false);
            newOrder.setEnabled(true);
            bill.setEnabled(true);
            orderList.setOrderList(this);
            orderList.notifyDataSetChanged();
            setTotalInfo();
            billRequestSts.setText("Bill Processing");
            billRequestSts.setVisibility(View.VISIBLE);
        } else {
            billRequestSts.setVisibility(View.INVISIBLE);
        }
        reset();
    }

    View.OnClickListener orderAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnOrderPlace:
                    if (orderMessage()) {
                        orderList.placeOrderList();
                        newOrder.setEnabled(false);
                        bill.setEnabled(true);
                        placeOrder.setText("UPDATE ORDER");
                        placeOrder.setEnabled(false);
                    }
                    orderList.notifyDataSetChanged();
                    refresh();
                    break;
                case R.id.btnNewOrder:
                    mydb.deleteAllOrderList();
                    mydb.deleteAllOrderDetails();
                    myIntent = new Intent(MainActivity.this, CustomerWelcome.class);
                    startActivity(myIntent);
                    break;
                case R.id.btnBill:
                    if (!orderList.getOrderMasterId().equalsIgnoreCase("")) {
                        orderList.RequestBill();
                        billRequestSts.setText("Bill Processing");
                        // mydb.updateOrderDetails(mydb.getOrderMasterId(), 5);
                        billRequestSts.setVisibility(View.VISIBLE);
                    }
                    placeOrder.setEnabled(false);
                    newOrder.setEnabled(true);
                    break;
            }
        }
    };

    public void reset() {
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);
    }

    public boolean orderMessage() {
        final String[] strMsg = new String[1];
        final boolean[] done = {false};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.order_message_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText txtMSG = (EditText) dialogView.findViewById(R.id.edit1);
        txtMSG.setHint("Write Message");
        dialogBuilder.setTitle("Message for Kitchen");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                strMsg[0] = txtMSG.getText().toString();
                orderList.pushNewOrderList(Token, tableId, strdeviceId, strMsg[0]);
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return done[0];
    }

    public void pushNotification(String mId, String menuName) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.error)
                        .setContentTitle("Dining Park Notification")
                        .setContentText(menuName + " Is Unavailable! Please Select Another Item.");
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(alarmSound);
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Integer.parseInt(mId), mBuilder.build());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            myIntent = new Intent(MainActivity.this, SearchMenuActivity.class);
            myIntent.putExtra("orderMasterId", orderList.getOrderMasterId());
            myIntent.putExtra("tableName", strTableNo);
            myIntent.putExtra("tableId", Integer.toString(tableId));
            startActivity(myIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            myIntent = new Intent(MainActivity.this, ImageSlideShow.class);
            startActivity(myIntent);

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_logout) {
            confirmPassword();
        } else if (id == R.id.nav_reset) {
            confirmPassword();
        } else if (id == R.id.nav_about) {
            myIntent = new Intent(MainActivity.this, np.com.diningpark.helpers.AdoutusActivity.class);
            startActivity(myIntent);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public boolean confirmPassword() {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.password_confirmation, null);
        dialogBuilder.setView(dialogView);

        dialogBuilder.setTitle("Confirm User and Password");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                EditText cfmUser = (EditText) dialogView.findViewById(R.id.cfmUser);
                String strUser = cfmUser.getText().toString();
                EditText cfmPassword = (EditText) dialogView.findViewById(R.id.cfmPassword);
                String cfmPsd = cfmPassword.getText().toString();

                if (isConnected()) {
                    new UserLoginTask(strUser, cfmPsd, strTableNo, strdeviceId, "Table").execute();
                } else {
                    showAlertDialog(MainActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }


            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return passwordResult;

    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    //TODO: Massage Input Dialog for Waitert Call

    public boolean messageDialog(String msg, String title) {
        passwordResult = false;
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.back_warning_dailog, null);
        dialogBuilder.setView(dialogView);

        final TextView edt = (TextView) dialogView.findViewById(R.id.edit1);
        edt.setText(msg);

        dialogBuilder.setTitle(title);
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                passwordResult = false;
            }

        }).setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                passwordResult = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
        return passwordResult;
    }

    private class menuCateJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(MainActivity.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            url = URL.ServerUrl + URL.MenuTitleList;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                    String MenuTitleId = jsonObject.optString("MenuTitleId");
                    String MenuTitleImageUrl = jsonObject.optString("MenuTitleImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTitleName");

                    if (i == 0) {
                        data = MenuTitleId;
                    }

                    menuCateAdapter.addMenuItem(MainActivity.this, MenuTitleName, MenuTitleImageUrl, Integer.parseInt(MenuTitleId));
                }
                menuCateAdapter.notifyDataSetChanged();
                menuCategoryId = Integer.parseInt(data);

                if (isConnected()) {
                    progressBar.show();
                    new menuJSONParse().execute();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //todo: Menu Item JSON.

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        private android.app.ProgressDialog pDialo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.MenuListByTitle + Integer.toString(menuCategoryId);

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                itemListImage.clearItem();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ArrayList<String> foodType = new ArrayList<String>();
                    foodType.clear();
                    JSONArray jsonFoodType = jsonObject.optJSONArray("FoodTypes");

                    for (int j = 0; j < jsonFoodType.length(); j++) {
                        JSONObject jsonFoodTypeObject = jsonFoodType.getJSONObject(j);
                        String ft = jsonFoodTypeObject.optString("FoodTypeId");
                        foodType.add(j, ft);
                    }

                    String MenuCalories = jsonObject.optString("MenuCalories");
                    String MenuDetail = jsonObject.optString("MenuDetail");
                    String MenuId = jsonObject.optString("MenuId");
                    String MenuIngredient = jsonObject.optString("MenuIngredient");
                    String MenuSuggestion = jsonObject.optString("MenuSuggestion");
                    String MenuImageUrl = jsonObject.optString("MenuImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTName");
                    String rate = jsonObject.optString("Rate");
                    String MenuTypeId = jsonObject.optString("MenuTypeId ");

                    Float fRate = Float.parseFloat(rate);
                    String strRate = String.format("%.0f", fRate);

                    itemListImage.addItem(MainActivity.this, Integer.parseInt(MenuId), MenuTitleName, foodType, MenuIngredient, MenuSuggestion, MenuImageUrl, strRate, MenuCalories, MenuDetail, MenuTypeId);

                }
                itemListImage.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class SENDMASSAGEJSONParse extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.SaveNewMessage;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                JSONObject jsonObject = new JSONObject();

                jsonObject.accumulate("CustomerMessage", cMsg);
                jsonObject.accumulate("ResTableId", tableId);


                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);

                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Token", Token);
                httpPost.setHeader("DeviceId", strdeviceId);

                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject json = new JSONObject(result);

                String Message = json.optString("Message");
                String MsgType = json.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), "Message Send " + Message,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), Message,
                            Toast.LENGTH_SHORT).show();
                }
            } catch (
                    JSONException e
                    )

            {
                e.printStackTrace();
            }
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;
        private final String mPassword;
        private final String mTableNumber;
        private final String mDeviceId;
        private final String mLoginType;

        UserLoginTask(String email, String password, String tableNumber, String deviceId, String loginAs) {
            mEmail = email;
            mPassword = password;
            mTableNumber = tableNumber;
            mDeviceId = deviceId;
            mLoginType = loginAs;
        }


        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.UserLogin;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();

                jsonObject.accumulate("UserName", mEmail);
                jsonObject.accumulate("Password", mPassword);
                jsonObject.accumulate("TableNumber", mTableNumber);
                jsonObject.accumulate("DeviceId", mDeviceId);
                jsonObject.accumulate("LoginType ", mLoginType);


                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();


                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            try {
                JSONObject json = new JSONObject(result);

                System.out.println(" Result of JSON : " + json);

                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success.") && MsgType.equalsIgnoreCase("Success")) {
                    System.gc();
                    if (autoUpdate != null) {
                        autoUpdate.cancel();
                        autoUpdate.purge();
                        autoUpdate = null;
                    }
                    mydb.deleteAllOrderList();
                    mydb.deleteAllOrderDetails();
                    editor.clear();
                    editor.commit();
                    myIntent = new Intent(MainActivity.this, WelcomeActivity.class);
                    finish();
                    startActivity(myIntent);
                } else {
                    messageDialog("Please don't try again !!!", "Password Wrong!");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }
}
