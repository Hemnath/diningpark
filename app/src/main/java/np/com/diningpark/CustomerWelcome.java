package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;

public class CustomerWelcome extends AppCompatActivity {

    Intent myIntent;
    EditText name,NoOfPerson,MobileNo;
    Button btnMenu;
    int count;
    DBHelper mydb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_welcome);

        mydb = new DBHelper(this);
        count = mydb.numberOfRows();
        if (count > 0) {
            myIntent = new Intent(CustomerWelcome.this, MainActivity.class);
            startActivity(myIntent);
        } else {
            name = (EditText) findViewById(R.id.editName);
            NoOfPerson = (EditText) findViewById(R.id.editPerson);
            btnMenu = (Button) findViewById(R.id.btnMenu);
            MobileNo = (EditText) findViewById(R.id.editMobile);

            btnMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    API_Setting.CustomerName = name.getText().toString();;
                    API_Setting.NoOfPerson = NoOfPerson.getText().toString();
                    API_Setting.MobileNo = MobileNo.getText().toString();

                    myIntent = new Intent(CustomerWelcome.this, MainActivity.class);
                    startActivity(myIntent);
                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        myIntent = new Intent(CustomerWelcome.this, TableNoActivity.class);
        startActivity(myIntent);

    }

    @Override
    public void onResume() {
        super.onResume();
        count = mydb.numberOfRows();
        if (count > 0) {
            myIntent = new Intent(CustomerWelcome.this, MainActivity.class);
            startActivity(myIntent);
        }
    }
}
