package np.com.diningpark;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import np.com.diningpark.fragment.GroupItemImageAdapter;
import np.com.diningpark.fragment.ImageAdapter;
import np.com.diningpark.fragment.LinearListView;
import np.com.diningpark.fragment.OrderListAdapter;
import np.com.diningpark.fragment.TableOrderListAdapter;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

public class TableDetailActivity extends AppCompatActivity {
    String tableName, orderMasterId;
    String orderCall;

    GridView menuGridview;
    LinearListView menuCategoryList;
    ListView OrderList;
    String name, price, calories, url, strQty, strSeqNum, menuIngredient, menuSuggestion;
    int Qty, item_id, seqNum;
    Intent myIntent;
    ImageAdapter itemListImage;
    GroupItemImageAdapter menuCateAdapter;
    TableOrderListAdapter orderList;
    TextView total, vat, serCharge, strVat, strSerCharge, payable, txtCustName, billRequestSts;
    int menuCategoryId;
    ProgressDialog progressBar;
    Button increase, decrease, increaseSeq, decreaseSeq, dismissButton, dismissOk, placeOrder, bill;
    TextView diagQty, diagSeqNo;

    public static final String DeviceId = "DeviceId";
    public static final String TableNo = "TableNo";
    public static final String ServiceChargeRate = "ServiceChargeRate";
    public static final String VatRate = "VatRate";

    String strdeviceId = "";
    String Token = "", strTableNo, tableID;
    float serviceChrgRate = 1, vatRate = 1;
    API_Setting URL;

    SharedPreferences sharedpreferences;

    DBHelper mydb;
    Dialog dialog;
    boolean passwordResult = false;

    ImageView searchView;
    TextView waiterName;
    TextView tableNo;
    Button back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_detail);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        URL = new API_Setting();

        strdeviceId = sharedpreferences.getString(DeviceId, null);
        Token = sharedpreferences.getString("TokenId", null);
        strTableNo = sharedpreferences.getString(TableNo, null);

        serviceChrgRate = Float.parseFloat(sharedpreferences.getString(ServiceChargeRate, "0"));
        vatRate = Float.parseFloat(sharedpreferences.getString(VatRate, "0"));
        mydb = new DBHelper(this);

        Intent intent = getIntent();

        orderMasterId = intent.getStringExtra("orderMasterId");
        tableName = intent.getStringExtra("tableName");
        tableID = intent.getStringExtra("tableId");
        orderCall = intent.getStringExtra("orderCall");


        txtCustName = (TextView) findViewById(R.id.custName);
        txtCustName.setText("Dining Park" + " ( Table No. " + tableName + " )");

        waiterName = (TextView) findViewById(R.id.waiterName);
        tableNo = (TextView) findViewById(R.id.tableNo);
        tableNo.setText("( Table No: " + tableName + " )");

        back = (Button) findViewById(R.id.btnBack);
        back.setOnClickListener(goBack);
        waiterName.setOnClickListener(goBack);

        total = (TextView) findViewById(R.id.txtTotal);
        vat = (TextView) findViewById(R.id.txtVat);
        serCharge = (TextView) findViewById(R.id.txtSercharge);
        payable = (TextView) findViewById(R.id.txtPayable);
        strVat = (TextView) findViewById(R.id.txtStrVat);
        strSerCharge = (TextView) findViewById(R.id.txtStrSercharge);

        //Dialog interface
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_dailog_layout);

        dismissButton = (Button) dialog.findViewById(R.id.button);
        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dismissOk = (Button) dialog.findViewById(R.id.add);

        increase = (Button) dialog.findViewById(R.id.btnIncrs);
        decrease = (Button) dialog.findViewById(R.id.btnDecrs);
        increaseSeq = (Button) dialog.findViewById(R.id.btnIncrsSeq);
        decreaseSeq = (Button) dialog.findViewById(R.id.btnDecrsSeq);
        diagQty = (TextView) dialog.findViewById(R.id.diagQty);
        diagSeqNo = (TextView) dialog.findViewById(R.id.diagSeqNo);

        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempQty = diagQty.getText().toString();
                int intQty = Integer.parseInt(tempQty);
                intQty++;
                diagQty.setText(String.valueOf(intQty));
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempQty = diagQty.getText().toString();
                int intQty = Integer.parseInt(tempQty);
                if (intQty > 1)
                    intQty--;
                diagQty.setText(String.valueOf(intQty));
            }
        });

        increaseSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = diagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                diagSeqNo.setText(String.valueOf(intSeq));
            }
        });
        decreaseSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = diagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                diagSeqNo.setText(String.valueOf(intSeq));
            }
        });


        menuGridview = (GridView) findViewById(R.id.itemGridView);
        menuCategoryList = (LinearListView) findViewById(R.id.horizontal_list);
        OrderList = (ListView) findViewById(R.id.orderList);

        menuCategoryList.setDividerThickness(getResources().getDimensionPixelSize(R.dimen.padding_small));

        menuCateAdapter = new GroupItemImageAdapter(this);
        menuCategoryList.setAdapter(menuCateAdapter);
        menuCategoryList.setOnItemClickListener(mListener);

        if (isConnected()) {
            new menuCateJSONParse().execute();
            if (API_Setting.helpingHand) {
                new HelpingHandsAvailableTable().execute();
            }
        }

        itemListImage = new ImageAdapter(this);
        menuGridview.setAdapter(itemListImage);

        orderList = new TableOrderListAdapter(this, orderMasterId, strdeviceId, Token);
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);

        placeOrder = (Button) findViewById(R.id.btnOrderPlace);
        bill = (Button) findViewById(R.id.btnBill);

        billRequestSts = (TextView) findViewById(R.id.billRequestSts);

        placeOrder.setOnClickListener(orderAction);
        bill.setOnClickListener(orderAction);

        searchView = (ImageView) findViewById(R.id.searchView);
        searchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myIntent = new Intent(TableDetailActivity.this, SearchMenuActivity.class);
                myIntent.putExtra("orderMasterId", orderMasterId);
                myIntent.putExtra("tableName", tableName);
                myIntent.putExtra("tableId", tableID);
                startActivity(myIntent);
            }
        });

        if (orderCall.equalsIgnoreCase("yes")) {
            orderList.setOldOrder();
        } else {
            refresh();
        }

        OrderList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                myIntent = new Intent(TableDetailActivity.this, EditOrderListActivity.class);
                myIntent.putExtra("parents", "waiter");
                myIntent.putExtra("orderMasterId", orderMasterId);
                myIntent.putExtra("tableName", tableName);
                myIntent.putExtra("tableId", tableID);
                startActivity(myIntent);
            }
        });


       /* OrderList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                TableOrderListAdapter.ViewHolder vh = (TableOrderListAdapter.ViewHolder) view.getTag();

                final int touchedX = (int) (vh.lastTouchedX + 0.5f);
                final int touchedY = (int) (vh.lastTouchedY + 0.5f);

                view.startDrag(null, new View.DragShadowBuilder(view) {
                    @Override
                    public void onProvideShadowMetrics(Point shadowSize, Point shadowTouchPoint) {
                        super.onProvideShadowMetrics(shadowSize, shadowTouchPoint);
                        shadowTouchPoint.x = touchedX;
                        shadowTouchPoint.y = touchedY;
                    }

                    @Override
                    public void onDrawShadow(Canvas canvas) {
                        super.onDrawShadow(canvas);
                    }
                }, view, 0);

                return true;
            }
        });

        OrderList.setOnDragListener(new View.OnDragListener() {

            @Override
            public boolean onDrag(View v, DragEvent event) {

                if (event.getAction() == DragEvent.ACTION_DROP) {

                }
                return true;
            }
        });
*/

        menuGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                item_id = itemListImage.getMenuId(position);
                String imageURL = String.valueOf(itemListImage.getImageURL(position));
                name = itemListImage.getItemName(position);
                price = itemListImage.getItemPrice(position);
                calories = itemListImage.getItemCalories(position);
                menuIngredient = itemListImage.getItemmenuIngredient(position);
                menuSuggestion = itemListImage.getItemmenuSuggestion(position);

                if (menuSuggestion.isEmpty() || menuSuggestion.equalsIgnoreCase("null")) {
                    menuSuggestion = "";
                }

                TextView txtdiaPrice = (TextView) dialog.findViewById(R.id.itemdPrice);
                txtdiaPrice.setText(price);
                TextView txtdiaCalories = (TextView) dialog.findViewById(R.id.itemDaiCalories);
                txtdiaCalories.setText(calories);

                TextView txtdiaName = (TextView) dialog.findViewById(R.id.itemdName);
                txtdiaName.setText(name);
                TextView txtMenuIngredient = (TextView) dialog.findViewById(R.id.productDesc);
                txtMenuIngredient.setText(menuIngredient);
                TextView txtMenuSuggestion = (TextView) dialog.findViewById(R.id.txtcalories);
                txtMenuSuggestion.setText(menuSuggestion);
                diagQty.setText("1");
                diagSeqNo.setText(String.valueOf(mydb.getMaxOldOrderSequence()));
                ImageView diaImage = (ImageView) dialog.findViewById(R.id.picture);
                com.squareup.picasso.Picasso.with(TableDetailActivity.this).load(imageURL).into(diaImage);
                dialog.show();

            }
        });

        dismissOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strQty = diagQty.getText().toString();
                strSeqNum = diagSeqNo.getText().toString();
                Qty = Integer.parseInt(strQty);
                seqNum = Integer.parseInt(strSeqNum);
                if (mydb.getOrderStatus() < 5) {
                    addOrderList();
                }
                dialog.dismiss();
                refresh();
            }
        });
        refresh();
    }

    View.OnClickListener goBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isConnected()) {
                if (API_Setting.helpingHand) {
                    API_Setting.helpingHand = false;
                    new HelpingHandsAvailableTable().execute();
                }
            } else {
                showAlertDialog(TableDetailActivity.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
            myIntent = new Intent(TableDetailActivity.this, WaiterMainActivity.class);
            startActivity(myIntent);
        }
    };

    LinearListView.OnItemClickListener mListener = new LinearListView.OnItemClickListener() {

        @Override
        public void onItemClick(LinearListView parent, View view, int position, long id) {
            menuCategoryId = (int) menuCateAdapter.getItemId(position);
            if (isConnected()) {
                progressBar.show();
                new menuJSONParse().execute();
            }
        }
    };

    public void setOrderMasterId(String MasterId) {
        orderMasterId = MasterId;
    }

    public void addOrderList() {
        int intCalories = Integer.parseInt(calories) * Qty;
        float fp = Float.parseFloat(price);
        mydb.insertOrderList(item_id, name, fp, Qty, Integer.toString(intCalories), "", seqNum, " ", mydb.geDeliveryTime(seqNum), 0, 0);
        orderList.setOrderList(this);
        orderList.notifyDataSetChanged();
        setTotalInfo();
        setTotalInfo();
        Toast.makeText(this, name + " Added on Order List",
                Toast.LENGTH_SHORT).show();
        refresh();
    }

    public void setTotalInfo() {
        float p, takeP, vt, sc, pay;
        p = mydb.getPriceSum();
        takeP = mydb.getTakeAwayPriceSum();
        sc = (p - takeP) * serviceChrgRate / 100;
        vt = (p + sc) * vatRate / 100;
        pay = p + vt + sc;
        total.setText(String.format("%.0f", p));
        vat.setText(String.format("%.0f", vt));
        serCharge.setText(String.format("%.0f", sc));
        strVat.setText(("Vat " + Float.toString(vatRate) + "% :"));
        strSerCharge.setText(("Services Charge " + Float.toString(serviceChrgRate) + "% :"));
        payable.setText(String.format("%.0f", pay));
    }

    public void reset() {
        OrderList.setAdapter(orderList);
        orderList.setOrderList(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (isConnected()) {
            if (API_Setting.helpingHand) {
                API_Setting.helpingHand = false;
                new HelpingHandsAvailableTable().execute();
            }
        }  else {
            showAlertDialog(TableDetailActivity.this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refresh();
        setTotalInfo();
        orderList.notifyDataSetChanged();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        refresh();
        setTotalInfo();
        orderList.notifyDataSetChanged();
    }

    public void refresh() {
        orderList.notifyDataSetChanged();
        if (mydb.haveOrderPlace() && mydb.getOrderStatus() < 5) {
            placeOrder.setText("UPDATE ORDER");
        } else {
            placeOrder.setText("PLACE ORDER");
        }
        if (mydb.haveNewOrder()) {
            placeOrder.setEnabled(true);
            bill.setEnabled(false);
        } else if (!mydb.haveNewOrder() && mydb.haveOrderPlace()) {
            bill.setEnabled(true);
        } else {
            placeOrder.setEnabled(false);
        }

        if (mydb.getOrderStatus() >= 5) {
            orderList.setOrderList(this);
            orderList.notifyDataSetChanged();
            setTotalInfo();
            bill.setEnabled(false);
            billRequestSts.setText("Bill Processing");
            billRequestSts.setVisibility(View.VISIBLE);
        } else {
            //bill.setEnabled(true);
            billRequestSts.setVisibility(View.INVISIBLE);
        }
        reset();
    }

    View.OnClickListener orderAction = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btnOrderPlace:
                    if (orderMasterId.equals("0") || orderMasterId.equals(""))
                        custNameAndMessage();
                    else
                        orderMessage();

                    placeOrder.setText("UPDATE ORDER");

                    refresh();
                    break;
                case R.id.btnBill:
                    if (!orderList.getOrderMasterId().equalsIgnoreCase("")) {
                        RequestBill();
                        billRequestSts.setText("Bill Processing");
                        billRequestSts.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    };

    public boolean orderMessage() {
        final String[] strMsg = new String[1];
        final boolean[] done = {false};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.order_message_dialog, null);
        dialogBuilder.setView(dialogView);
        final EditText txtMSG = (EditText) dialogView.findViewById(R.id.edit1);
        txtMSG.setHint("Write Message");
        dialogBuilder.setTitle("Message for Kitchen");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                strMsg[0] = txtMSG.getText().toString();
                orderList.pushNewOrderList(Token, Integer.parseInt(tableID), strdeviceId, strMsg[0], strMsg[0], strMsg[0], strMsg[0]);
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return done[0];
    }

    public boolean custNameAndMessage() {
        final String[] strMsg = new String[4];
        final boolean[] done = {false};

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.emp_name_message, null);
        dialogBuilder.setView(dialogView);
        final EditText txtMSG = (EditText) dialogView.findViewById(R.id.edit1);
        txtMSG.setHint("Message");
        final EditText txtName = (EditText) dialogView.findViewById(R.id.edit2);
        txtName.setHint("Name");
        final EditText txtMobile = (EditText) dialogView.findViewById(R.id.edit3);
        txtMobile.setHint("Mobile No.");
        final EditText txtPerson = (EditText) dialogView.findViewById(R.id.edit4);
        txtPerson.setHint("No. of Person");
        dialogBuilder.setTitle("Customer Details");
        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                strMsg[0] = txtMSG.getText().toString();
                strMsg[1] = txtName.getText().toString();
                strMsg[2] = txtMobile.getText().toString();
                strMsg[3] = txtPerson.getText().toString();
                orderList.pushNewOrderList(Token, Integer.parseInt(tableID), strdeviceId, strMsg[0], strMsg[1], strMsg[2], strMsg[3]);
                done[0] = true;
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();

        return done[0];
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_search) {
            myIntent = new Intent(this, SearchMenuActivity.class);
            myIntent.putExtra("orderMasterId", orderList.getOrderMasterId());
            myIntent.putExtra("tableName", Integer.toString(Integer.parseInt(tableID)));
            startActivity(myIntent);
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    public boolean RequestBill() {

        if (isConnected()) {
            new SENDMASSAGEJSONParse().execute();
        } else {
            showAlertDialog(this, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        return true;
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private class menuCateJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(TableDetailActivity.this);
            progressBar.setMessage("Downloading...");
            progressBar.show();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            url = URL.ServerUrl + URL.MenuTitleList;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                    String MenuTitleId = jsonObject.optString("MenuTitleId");
                    String MenuTitleImageUrl = jsonObject.optString("MenuTitleImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTitleName");

                    if (i == 0) {
                        data = MenuTitleId;
                    }

                    menuCateAdapter.addMenuItem(TableDetailActivity.this, MenuTitleName, MenuTitleImageUrl, Integer.parseInt(MenuTitleId));
                }
                menuCateAdapter.notifyDataSetChanged();
                menuCategoryId = Integer.parseInt(data);

                if (isConnected()) {
                    progressBar.show();
                    new menuJSONParse().execute();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    //todo: Menu Item JSON.

    private class menuJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        private android.app.ProgressDialog pDialo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.MenuListByTitle + Integer.toString(menuCategoryId);

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                if (progressBar.isShowing()) {
                    progressBar.dismiss();
                }
                JSONArray jsonArray = json.optJSONArray("List");
                itemListImage.clearItem();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ArrayList<String> foodType = new ArrayList<String>();
                    foodType.clear();
                    JSONArray jsonFoodType = jsonObject.optJSONArray("FoodTypes");

                    for (int j = 0; j < jsonFoodType.length(); j++) {
                        JSONObject jsonFoodTypeObject = jsonFoodType.getJSONObject(j);
                        String ft = jsonFoodTypeObject.optString("FoodTypeId");
                        foodType.add(j, ft);
                    }

                    String MenuCalories = jsonObject.optString("MenuCalories");
                    String MenuDetail = jsonObject.optString("MenuDetail");
                    String MenuId = jsonObject.optString("MenuId");
                    String MenuIngredient = jsonObject.optString("MenuIngredient");
                    String MenuSuggestion = jsonObject.optString("MenuSuggestion");
                    String MenuImageUrl = jsonObject.optString("MenuImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTName");
                    String rate = jsonObject.optString("Rate");
                    String MenuTypeId = jsonObject.optString("MenuTypeId ");

                    Float fRate = Float.parseFloat(rate);
                    String strRate = String.format("%.0f", fRate);

                    itemListImage.addItem(TableDetailActivity.this, Integer.parseInt(MenuId), MenuTitleName, foodType, MenuIngredient, MenuSuggestion, MenuImageUrl, strRate, MenuCalories, MenuDetail, MenuTypeId);

                }
                itemListImage.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class SENDMASSAGEJSONParse extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.SaveNewMessage;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                JSONObject jsonObject = new JSONObject();

                System.out.println("Table Id = " + tableID);


                jsonObject.accumulate("CustomerMessage", "Bill Print Please");
                jsonObject.accumulate("ResTableId", tableID);


                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);

                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Token", Token);
                httpPost.setHeader("DeviceId", strdeviceId);


                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject json = new JSONObject(result);

                String Message = json.optString("Message");
                String MsgType = json.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String url = URL.ServerUrl + URL.UpdateOrderStatus + orderMasterId;
                    new PushBillRequest().execute(url);

                    Toast.makeText(TableDetailActivity.this, "Bill Requested" + Message,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(TableDetailActivity.this, Message,
                            Toast.LENGTH_SHORT).show();
                }
            } catch (
                    JSONException e
                    )

            {
                e.printStackTrace();
            }
        }
    }

    public String PushBillRequestPOST(String url) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OrderStatusId", "9");
            jsonObject.put("OrderStatusName", "Bill Request");

            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", Token);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);
            httpPost.setHeader("Token", Token);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushBillRequest extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return PushBillRequestPOST(urls[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String OrderStatusId = json.optString("OrderStatusId");

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class HelpingHandsAvailableTable extends AsyncTask<String, String, JSONObject> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            url = URL.ServerUrl + URL.SetResetHelpingHandStatus + tableID;
            JSONParserCustom jParser = new JSONParserCustom();
            JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {

        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }
}