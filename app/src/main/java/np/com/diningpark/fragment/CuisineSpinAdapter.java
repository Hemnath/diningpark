package np.com.diningpark.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;

/**
 * Created by ik890 on 2/17/2016.
 */
public class CuisineSpinAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    Context mainContext;

    public CuisineSpinAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        String imageURL="http://202.166.200.238:8000/Content/Menu/MenuTitle/MenuTitle_4747nepali-momo-1.gif";
        items.add(new Item("Idian Continantal",imageURL,1));
        items.add(new Item("Chinese Continantal",imageURL,1));
        items.add(new Item("Frence ",imageURL,1));
        items.add(new Item("Drik and Bekary",imageURL,1));
        items.add(new Item("Other",imageURL,1));
        mainContext = context;
    }

    public void addCuisineItem(Context context,String MenuName,String imageURL,int menuCuisId) {
        inflater = LayoutInflater.from(context);
        items.add(new Item(MenuName,imageURL,menuCuisId));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).CuisineId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = inflater.inflate(R.layout.cuisine_spin_layout, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.text, v.findViewById(R.id.text));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.text);

        Item item = (Item) getItem(i);
        String imageURLFull=item.ImageURL;
        com.squareup.picasso.Picasso.with(mainContext).load(imageURLFull).into(picture);
        name.setText(item.name);

        return v;
    }

    private class Item {
        final String name;
        final String ImageURL;
        final int CuisineId;

        Item(String name, String ImageURL, int CuisineId) {
            this.name = name;
            this.ImageURL=ImageURL;
            this.CuisineId = CuisineId;
        }
    }


}