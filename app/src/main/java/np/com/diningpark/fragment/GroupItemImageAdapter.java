package np.com.diningpark.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;

/**
 * Created by Technomax-Server on 1/5/2016.
 */
public class GroupItemImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    org.json.JSONArray jsonList = null;
    Context mainContext;

    public GroupItemImageAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mainContext = context;
    }

    public void addMenuItem(Context context,String MenuName,String imageURL,int menuCateId) {
        inflater = LayoutInflater.from(context);
        items.add(new Item(MenuName,imageURL,menuCateId));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).menuCategoryId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name;

        if (v == null) {
            v = inflater.inflate(R.layout.group_list_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.text, v.findViewById(R.id.text));
        }

        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.text);

        Item item = (Item) getItem(i);
        String imageURLFull=item.ImageURL;
        //com.squareup.picasso.Picasso.with(mainContext).load(imageURLFull).into(picture);
        name.setText(item.name);

        return v;
    }

    private class Item {
        final String name;
        final String ImageURL;
        final int menuCategoryId;

        Item(String name, String ImageURL, int menuCategoryId) {
            this.name = name;
            this.ImageURL=ImageURL;
            this.menuCategoryId = menuCategoryId;
        }
    }


}