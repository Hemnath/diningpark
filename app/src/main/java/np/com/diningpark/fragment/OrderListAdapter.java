package np.com.diningpark.fragment;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.MainActivity;
import np.com.diningpark.R;
import np.com.diningpark.SearchMenuActivity;
import np.com.diningpark.TableNoActivity;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;


public class OrderListAdapter extends BaseAdapter {
    private List<Object> items = new ArrayList<Object>();
    private LayoutInflater inflater;
    Context mContext;
    View v;
    private DBHelper mydb;
    private String tokenId, strdeviceId, url, orderMsg;
    private int tableId;
    private static String orderMasterId;
    private API_Setting URL;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_DIVIDER = 1;

    public OrderListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mydb = new DBHelper(context);
        mContext = context;
        URL = new API_Setting();
    }

    public int setOrderList(Context context) {
        inflater = LayoutInflater.from(context);
        ArrayList<Integer> array_list;
        int returnValue = 1;
        items.clear();
        int cnt = 0;
        for (int sq = 1; sq <= mydb.getMaxOrderSequence(); sq++) {
            items.add(cnt, " Order Sequence " + sq + " ");
            array_list = mydb.getAllOrderList1(sq);
            int count = array_list.size();
            for (int i = 0; i < count; i++) {
                cnt++;
                Cursor rs = mydb.getData(array_list.get(i));
                rs.moveToFirst();
                String strId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ID));
                String strItemId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ITEMID));
                String strName = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_NAME));
                float strRate = rs.getFloat(rs.getColumnIndex(DBHelper.COLUMN_PRICE));
                String strQty = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_QTY));
                int StrCalories = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_CALORIES));
                int strOrderStatus = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_ORDERSTATUS));
                String orderDetailId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OREDERDETAILID));
                int intOrderSequence = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_OrderSequence));
                int orderDeliveryTime = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_DeliveryTime));
                int takeAway = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_TakeAway));
                String strOrderMessage = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OrderMessage));
                float totalPrice = strRate * Integer.parseInt(strQty);
                items.add(cnt, new Item(strId, strItemId, strName, Float.toString(totalPrice), Float.toString(strRate), Integer.toString(StrCalories), strQty, orderDetailId, takeAway, intOrderSequence, strOrderMessage, orderDeliveryTime, strOrderStatus));
                rs.close();
            }
            cnt++;
        }
        return returnValue;

    }

    //TODO : SEQUENCE NUMBER
    public boolean placeOrderList() {
        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, item.orderDetailId, item.orderSequence, item.orderMessage, item.deliveryTime, item.takeAway, item.orderStatus);
            }
        }
        setOrderList(mContext);
        notifyDataSetChanged();
        return true;
    }


    public void GetTableOrder(String token, int table, String deviceId) {
        tokenId = token;
        tableId = table;
        strdeviceId = deviceId;
        if (isConnected()) {
            new GetTableOrderJSONParse().execute("");
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }

    }

    //TODO : SEQUENCE NUMBER
    public void updateOrderItemList(String mId, String menuName, String price, String Qty, String Calories, String OrderItemsId, int sequenceNumber, String message, int deliveryTime, int takeAway, String OrderStatusId) {

        if (OrderStatusId.equalsIgnoreCase("6")) {
            if (!(mydb.getIsOldOrder(OrderItemsId) == 6)) {
                ((MainActivity) mContext).pushNotification(mId, menuName);
                float fprice = Float.parseFloat(price);
                mydb.insertOldOrderList(Integer.parseInt(mId), menuName, fprice, Integer.parseInt(Qty), Calories, OrderItemsId, sequenceNumber, message, deliveryTime, takeAway, Integer.parseInt(OrderStatusId));
            }
        } else {
            float fprice = Float.parseFloat(price);
            mydb.insertOldOrderList(Integer.parseInt(mId), menuName, fprice, Integer.parseInt(Qty), Calories, OrderItemsId, sequenceNumber, message, deliveryTime, takeAway, Integer.parseInt(OrderStatusId));
        }
        setOrderList(mContext);
        notifyDataSetChanged();
    }

    public void pushNewOrderList(String token, int table, String deviceId, String msg) {
        tokenId = token;
        tableId = table;
        strdeviceId = deviceId;
        if (orderMasterId.equals("")) {
            orderMsg = msg;
            url = URL.ServerUrl + URL.PostOrdere;
            if (isConnected()) {
                new PushOrderList().execute(url);
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        } else {

            if (isConnected()) {
                placeNewOrderItem();
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

        }
        if (mContext.getClass() == MainActivity.class) {
            ((MainActivity) mContext).reset();
        } else if (mContext.getClass() == SearchMenuActivity.class) {
            ((SearchMenuActivity) mContext).reset();
        }
    }

    public void pushNewOrderList1(String token, int table, String deviceId, String MasterId, String msg) {
        tokenId = token;
        tableId = table;
        strdeviceId = deviceId;
        orderMasterId = MasterId;

        if (orderMasterId.equals("") || orderMasterId.equals("0")) {
            url = URL.ServerUrl + URL.PostOrdere;
            orderMsg = msg;
            if (isConnected()) {
                new PushOrderList().execute(url);
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        } else {

            if (isConnected()) {
                placeNewOrderItem();
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

        }
        if (mContext.getClass() == MainActivity.class) {
            ((MainActivity) mContext).reset();
        } else if (mContext.getClass() == SearchMenuActivity.class) {
            ((SearchMenuActivity) mContext).reset();
        }
    }

    public void placeNewOrderItem() {
        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                if (item.orderStatus == 0) {
                    url = URL.ServerUrl + URL.AddNewOrderItem;
                    if (isConnected()) {
                        Boolean blnTakeAway = false;
                        if (item.takeAway == 1)
                            blnTakeAway = true;
                        new PushNewOrderItem(item.itemId, item.qty, item.orderSequence, item.orderMessage, item.deliveryTime, blnTakeAway).execute(url);
                    } else {
                        showAlertDialog(mContext, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                }
            }
        }
        if (mContext.getClass() == MainActivity.class) {
            ((MainActivity) mContext).reset();
        } else if (mContext.getClass() == SearchMenuActivity.class) {
            ((SearchMenuActivity) mContext).reset();
        }

    }

    public boolean RequestBill() {

        if (isConnected()) {
            new SENDMASSAGEJSONParse().execute();
            url = URL.ServerUrl + URL.SaveNewMessage;
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
        return true;
    }

    public void OrderStatus() {
        if (isConnected()) {
            new OrderDetailsJSONParse().execute("");
        } else {

            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    //TODO : SEQUENCE NUMBER
    public void updateOrderStatus(String mId, String OrderStatusId, String OrderItemsId) {

        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                if (item.itemId.equalsIgnoreCase(mId) && item.orderDetailId.equalsIgnoreCase(OrderItemsId)) {

                    mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, OrderItemsId, item.orderSequence, item.orderMessage, item.deliveryTime, item.takeAway, Integer.parseInt(OrderStatusId));

                } else if (item.itemId.equalsIgnoreCase(mId) && item.orderDetailId.equalsIgnoreCase("")) {
                    mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, OrderItemsId, item.orderSequence, item.orderMessage, item.deliveryTime, item.takeAway, Integer.parseInt(OrderStatusId));
                }
            }
        }
    }

    public boolean orderSequence(final Item item, int sq) {
        final boolean[] done = {false};
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final View dialogView = inflater.inflate(R.layout.order_sequence, null);
        dialogBuilder.setView(dialogView);

        final TextView txtDiagSeqNo = (TextView) dialogView.findViewById(R.id.diagSeqNo);
        txtDiagSeqNo.setText(String.valueOf(sq));

        Button btnDecrsSeq = (Button) dialogView.findViewById(R.id.btnDecrsSeq);
        Button btnIncrsSeq = (Button) dialogView.findViewById(R.id.btnIncrsSeq);

        btnIncrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });
        btnDecrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });

        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, item.orderDetailId, intSeq, item.orderMessage, mydb.geDeliveryTime(intSeq), item.takeAway, item.orderStatus);
                if (mContext.getClass() == MainActivity.class) {
                    ((MainActivity) mContext).reset();
                } else if (mContext.getClass() == SearchMenuActivity.class) {
                    ((SearchMenuActivity) mContext).reset();
                }
                done[0] = true;
            }
        });


        AlertDialog b = dialogBuilder.create();
        b.show();
        return done[0];
    }


    public String getOrderMasterId() {
        return orderMasterId;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Item) {
            return TYPE_ITEM;
        }
        return TYPE_DIVIDER;
    }

    @Override
    public boolean isEnabled(int position) {
        return (getItemViewType(position) == TYPE_ITEM);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        v = view;
        ViewHolder vh;
        int type = getItemViewType(i);
        if (v == null) {
            v = inflater.inflate(R.layout.order_list_item, viewGroup, false);
            vh = new ViewHolder(v.findViewById(R.id.dragBtn), v.findViewById(R.id.tvName), v.findViewById(R.id.item_id), v.findViewById(R.id.tvPrice), v.findViewById(R.id.tvRate), v.findViewById(R.id.tvQty), v.findViewById(R.id.tvSectionName), v.findViewById(R.id.sectionHeader), v.findViewById(R.id.sectionItem));
            v.setTag(vh);
        } else {
            vh = (ViewHolder) v.getTag();
        }
        switch (type) {
            case TYPE_DIVIDER:

                vh.sectionHeader.setVisibility(View.VISIBLE);
                vh.sectionItem.setVisibility(View.GONE);

                String titleString = (String) getItem(i);
                vh.tvSectionName.setText(titleString);
                break;
            case TYPE_ITEM:
                final Item item = (Item) getItem(i);

                vh.sectionHeader.setVisibility(View.GONE);
                vh.sectionItem.setVisibility(View.VISIBLE);

                int color;
                color = Color.GREEN;
                if (item.orderStatus == 0) {
                    vh.dragBtn.setVisibility(View.VISIBLE);
                    color = Color.parseColor("#4CAF5C");
                } else if (item.orderStatus == 1) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.MAGENTA;
                } else if (item.orderStatus == 2) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.MAGENTA;
                } else if (item.orderStatus == 3) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GREEN;
                } else if (item.orderStatus == 4) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GREEN;
                } else if (item.orderStatus == 5) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GRAY;
                } else if (item.orderStatus == 6) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.RED;
                }
                vh.name.setTextColor(color);
                vh.price.setTextColor(color);
                vh.rate.setTextColor(color);
                vh.qty.setTextColor(color);

                vh.name.setText(item.name);
                Float fRate = Float.parseFloat(item.rate);
                String strRate = String.format("%.0f", fRate);
                Float fPrice = Float.parseFloat(item.price);
                String strPrice = String.format("%.0f", fPrice);
                vh.rate.setText(strRate);
                vh.price.setText(strPrice);
                vh.qty.setText(item.qty);
                vh.id.setText(item.id);

                final int sq = item.orderSequence;
                vh.dragBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderSequence(item, sq);
                    }
                });
                break;
        }
        return v;
    }

    public static class ViewHolder {
        public ImageView dragBtn;
        public TextView name;
        public TextView id;
        public TextView price;
        public TextView rate;
        public TextView qty;
        public TextView tvSectionName;
        public LinearLayout sectionHeader;
        public LinearLayout sectionItem;

        public ViewHolder(View v1, View v2, View v3, View v4, View v5, View v6, View v7, View v8, View v9) {
            dragBtn = (ImageView) v1;
            name = (TextView) v2;
            id = (TextView) v3;
            price = (TextView) v4;
            rate = (TextView) v5;
            qty = (TextView) v6;
            tvSectionName = (TextView) v7;
            sectionHeader = (LinearLayout) v8;
            sectionItem = (LinearLayout) v9;
        }
    }


    private class Item {
        final String id;
        final String itemId;
        final String name;
        final String price;
        final String rate;
        final String calories;
        final String qty;
        final String orderDetailId;
        final int takeAway;
        final int orderStatus;
        final int orderSequence;
        final String orderMessage;
        final int deliveryTime;

        Item(String id, String itemId, String name, String price, String rate, String calories, String qty, String OrderItemsId, int takeAway, int orderSequence, String orderMessage, int deliveryTime, int status) {
            this.id = id;
            this.itemId = itemId;
            this.name = name;
            this.price = price;
            this.rate = rate;
            this.calories = calories;
            this.qty = qty;
            this.takeAway = takeAway;
            this.orderStatus = status;
            this.orderDetailId = OrderItemsId;
            this.orderSequence = orderSequence;
            this.orderMessage = orderMessage;
            this.deliveryTime = deliveryTime;
        }
    }

    private boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    private void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private class GetTableOrderJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            url = URL.ServerUrl + URL.GetTableOrder;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, tokenId);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {

                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String strOrderMasterId = json.optString("OrderMasterId");
                    String InvoiceNo = json.optString("InvoiceNo");
                    String OrderStatusId1 = json.optString("OrderStatusId");

                    mydb.insertOrderDetails(orderMasterId, InvoiceNo, Integer.parseInt(OrderStatusId1));
                    orderMasterId = strOrderMasterId;

                    mydb.deleteAllOldOrderList();

                    JSONArray jsonArray = json.optJSONArray("ItemList");

                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObjectLst = jsonArray.getJSONObject(i);

                        String MenuId = jsonObjectLst.optString("MenuId");
                        String MenuName = jsonObjectLst.optString("MenuName");
                        String OrderStatusId = jsonObjectLst.optString("OrderStatusId");
                        String OrderItemsId = jsonObjectLst.optString("OrderItemsId");
                        String Rate = jsonObjectLst.optString("Rate");
                        String Calories = jsonObjectLst.optString("Calories");
                        String Quantity = jsonObjectLst.optString("Quantity");
                        int DeliveryTime = jsonObjectLst.optInt("DeliveryTime");
                        int SequenceNumber = jsonObjectLst.optInt("SequenceNumber");
                        String orderMessage = jsonObjectLst.optString("Message");
                        boolean takeAway = jsonObjectLst.optBoolean("TakeAway");
                        int intTakeAway = 0;
                        if (takeAway) {
                            intTakeAway = 1;
                        }
                        updateOrderItemList(MenuId, MenuName, Rate, Quantity, Calories, OrderItemsId, SequenceNumber, orderMessage, DeliveryTime, intTakeAway, OrderStatusId);
                    }

                } else if (Message.equalsIgnoreCase("Empty table") && MsgType.equalsIgnoreCase("Success")) {
                    if (mydb.haveOrderPlace()) {
                        mydb.deleteAllOrderList();
                        mydb.deleteAllOrderDetails();
                        Intent myIntent = new Intent(mContext, TableNoActivity.class);
                        mContext.startActivity(myIntent);
                    } else {
                        mydb.deleteAllOrderDetails();
                        orderMasterId = "";
                    }
                } else {
                    if (mydb.haveOrderPlace()) {
                        mydb.updateOrderDetails(mydb.getOrderMasterId(), 5);
                        ((MainActivity) mContext).refresh();
                    } else {
                        mydb.deleteAllOrderDetails();
                        orderMasterId = "";
                    }
                }
                ((MainActivity) mContext).refresh();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private String POST(String url) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();

            for (int i = 0; i < items.size(); i++) {
                JSONObject jsonList = new JSONObject();
                if (getItem(i) instanceof Item) {
                    final Item item = (Item) getItem(i);
                    jsonList.put("MenuId", Integer.parseInt(item.itemId));
                    jsonList.put("Quantity", Integer.parseInt(item.qty));
                    jsonList.put("DeliveryTime", item.deliveryTime);
                    jsonList.put("Message", item.orderMessage);
                    jsonList.put("SequenceNumber", item.orderSequence);
                    if (item.takeAway == 1)
                        jsonList.put("TakeAway", true);
                    else
                        jsonList.put("TakeAway", false);
                    jsonArray.put(jsonList);
                }
            }

            jsonObject.put("ResTableId", tableId);
            jsonObject.put("CustomerName", API_Setting.CustomerName);
            jsonObject.put("MobileNumber", API_Setting.MobileNo);
            jsonObject.put("Persons", Integer.parseInt(API_Setting.NoOfPerson));
            jsonObject.put("CustomerMessage", orderMsg);

            jsonObject.put("ItemList", jsonArray);

            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            System.out.println("Result of JSON 2 = " + json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushOrderList extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            System.out.println("URL = " + urls[0]);
            return POST(urls[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            try {

                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    orderMasterId = json.optString("OrderMasterId");

                    if (mContext.getClass() == MainActivity.class) {
                        ((MainActivity) mContext).reset();
                    } else if (mContext.getClass() == SearchMenuActivity.class) {
                        ((SearchMenuActivity) mContext).setOrderMasterId(orderMasterId);
                        ((SearchMenuActivity) mContext).reset();
                    }
                    String InvoiceNo = json.optString("InvoiceNo");

                    mydb.insertOrderDetails(orderMasterId, InvoiceNo, 1);
                    OrderStatus();

                    Toast.makeText(mContext, "Order Place",
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mContext, "Error In Order Place",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private String NewORDERPOST(String url, String newOrderMenuId, String newOrderMenuQty, int SequenceNumber, String OrderMessage, int DeliveryTime, boolean takeAway) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("MenuId", newOrderMenuId);
            jsonObject.put("OrderMasterId", orderMasterId);
            jsonObject.put("Quantity", newOrderMenuQty);
            jsonObject.put("DeliveryTime", DeliveryTime);
            jsonObject.put("Message", OrderMessage);
            jsonObject.put("SequenceNumber", SequenceNumber);
            jsonObject.put("TakeAway", takeAway);
            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushNewOrderItem extends AsyncTask<String, Void, String> {
        String OrderMenuId;
        String OrderMenuQty;
        int SequenceNumber;
        String OrderMessage;
        int DeliveryTime;
        boolean TakeAway;

        public PushNewOrderItem(String OrderMenuId1, String OrderMenuQty1, int sequenceNumber, String orderMessage, int deliveryTime, boolean takeAway) {
            OrderMenuId = OrderMenuId1;
            OrderMenuQty = OrderMenuQty1;
            SequenceNumber = sequenceNumber;
            DeliveryTime = deliveryTime;
            OrderMessage = orderMessage;
            TakeAway = takeAway;
        }

        @Override
        protected String doInBackground(String... urls) {
            return NewORDERPOST(urls[0], OrderMenuId, OrderMenuQty, SequenceNumber, OrderMessage, DeliveryTime, TakeAway);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");
                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String strMenuId = json.optString("MenuId");
                    orderMasterId = json.optString("OrderMasterId");
                    String OrderItemsId = json.optString("OrderItemsId");

                    updateOrderStatus(strMenuId, "1", OrderItemsId);

                    Toast.makeText(mContext, "New Order Item Place",
                            Toast.LENGTH_SHORT).show();
                    if (mContext.getClass() == MainActivity.class) {
                        ((MainActivity) mContext).reset();
                    } else if (mContext.getClass() == SearchMenuActivity.class) {
                        ((SearchMenuActivity) mContext).reset();
                    }
                } else {
                    Toast.makeText(mContext, "Error In Order Place",
                            Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class OrderDetailsJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            orderMasterId = mydb.getOrderMasterId();
            url = URL.ServerUrl + URL.OrderDetail + orderMasterId;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();

            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, tokenId);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String MenuId = jsonObject.optString("MenuId");
                    String OrderStatusId = jsonObject.optString("OrderStatusId");
                    String OrderItemsId = jsonObject.optString("OrderItemsId");

                    updateOrderStatus(MenuId, OrderStatusId, OrderItemsId);

                    if (mContext.getClass() == MainActivity.class) {
                        ((MainActivity) mContext).reset();
                    } else if (mContext.getClass() == SearchMenuActivity.class) {
                        ((SearchMenuActivity) mContext).reset();
                    }

                }
                setOrderList(mContext);
                notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String PushBillRequestPOST(String url) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            JSONObject jsonObject = new JSONObject();

            jsonObject.put("OrderStatusId", "9");
            jsonObject.put("OrderStatusName", "Bill Request");

            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);
            httpPost.setHeader("Token", tokenId);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushBillRequest extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return PushBillRequestPOST(urls[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String OrderStatusId = json.optString("OrderStatusId");

                } else {

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class SENDMASSAGEJSONParse extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.SaveNewMessage;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                JSONObject jsonObject = new JSONObject();

                jsonObject.accumulate("CustomerMessage", "Bill Print Please");
                jsonObject.accumulate("ResTableId", tableId);


                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);

                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Token", tokenId);
                httpPost.setHeader("DeviceId", strdeviceId);


                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject json = new JSONObject(result);

                String Message = json.optString("Message");
                String MsgType = json.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String url = URL.ServerUrl + URL.UpdateOrderStatus + orderMasterId;
                    new PushBillRequest().execute(url);

                    Toast.makeText(mContext, "Bill Requested" + Message,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, Message,
                            Toast.LENGTH_SHORT).show();
                }
            } catch (
                    JSONException e
                    )

            {
                e.printStackTrace();
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }

}