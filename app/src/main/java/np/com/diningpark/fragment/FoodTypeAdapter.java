package np.com.diningpark.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;

/**
 * Created by ik890 on 4/17/2016.
 */
public class FoodTypeAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    org.json.JSONArray jsonList = null;
    Context mainContext;

    public FoodTypeAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mainContext = context;

        addFoodType(R.drawable.healthey, 1);
        addFoodType(R.drawable.delicious, 1);
        addFoodType(R.drawable.spicy_red,1);
        addFoodType(R.drawable.veggie, 1);
        addFoodType(R.drawable.non_veggie, 1);
    }

    public void addFoodType(int image, int menuCateId) {
        items.add(new Item(image, menuCateId));
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).menuCategoryId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView foodCategory;
        TextView name;

        if (v == null) {
            v = inflater.inflate(R.layout.food_type_list, viewGroup, false);
            v.setTag(R.id.foodCategory, v.findViewById(R.id.foodCategory));
        }

        foodCategory = (ImageView) v.getTag(R.id.foodCategory);

        Item item = (Item) getItem(i);
        foodCategory.setImageResource(item.Image);

        return v;
    }

    private class Item {
        final int Image;
        final int menuCategoryId;

        Item(int ImageURL, int menuCategoryId) {
            this.Image = ImageURL;
            this.menuCategoryId = menuCategoryId;
        }
    }


}