package np.com.diningpark.fragment;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import np.com.diningpark.R;
import np.com.diningpark.helpers.DBHelper;

/**
 * Created by ik890 on 5/12/2016.
 */
public class WaiterMsgAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    DBHelper mydb;

    public WaiterMsgAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        mydb = new DBHelper(mContext);
    }

    public void addMSG(String InvoiceNo, String Message, String OfflineMessageId, String OrderMasterId, String SentTime, String TableNumber, boolean isNew) {
        items.add(new Item(InvoiceNo, Message, OfflineMessageId, OrderMasterId, SentTime, TableNumber, isNew));
    }

    public void clear() {
        items.clear();
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView txtTableNumber, txtDate, txtMessage;

        if (v == null) {
            v = inflater.inflate(R.layout.waiter_msg_items, viewGroup, false);
            v.setTag(R.id.TableNumber, v.findViewById(R.id.TableNumber));
            v.setTag(R.id.date, v.findViewById(R.id.date));
            v.setTag(R.id.Message, v.findViewById(R.id.Message));

        }

        txtTableNumber = (TextView) v.getTag(R.id.TableNumber);
        txtDate = (TextView) v.getTag(R.id.date);
        txtMessage = (TextView) v.getTag(R.id.Message);

        Item item = (Item) getItem(i);

        int isNew = mydb.getIsNew(item.OfflineMessageId);
        if (isNew == 0) {
            txtTableNumber.setTextColor(Color.RED);
            txtDate.setTextColor(Color.RED);
            txtMessage.setTextColor(Color.RED);
        }

        txtTableNumber.setText(item.TableNumber);

        String json = item.SentTime;
        String timeString = json.substring(json.indexOf("(") + 1, json.indexOf(")"));
        String[] timeSegments = timeString.split("\\+");

        int timeZoneOffSet = Integer.valueOf(timeSegments[1]) * 36000;
        long millis = Long.valueOf(timeSegments[0]);
        Date time = new Date(millis + timeZoneOffSet);
        String dateTime = time.toString();

        txtDate.setText(dateTime);
        txtMessage.setText(item.Message);

        return v;
    }

    private class Item {
        final String InvoiceNo, Message, OfflineMessageId, OrderMasterId, SentTime, TableNumber;
        boolean isNew;


        Item(String InvoiceNo, String Message, String OfflineMessageId, String OrderMasterId, String SentTime, String TableNumber, boolean isNew) {
            this.InvoiceNo = InvoiceNo;
            this.Message = Message;
            this.OfflineMessageId = OfflineMessageId;
            this.OrderMasterId = OrderMasterId;
            this.SentTime = SentTime;
            this.TableNumber = TableNumber;
            this.isNew = isNew;

        }
    }
}
