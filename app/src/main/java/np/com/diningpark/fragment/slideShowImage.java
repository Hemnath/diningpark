package np.com.diningpark.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;

import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

/**
 * Created by ik890 on 4/26/2016.
 */
public class slideShowImage extends PagerAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater layoutInflater;

    String tokenId, deviceId;
    String tableName, orderMasterId, tableID;

    TextView btnAdd, rate;
    TextView Qty;
    DBHelper mydb;
    Button increase, decrease;
    API_Setting URL;

    public slideShowImage(Context context, String token, String device) {

        mContext = context;
        URL = new API_Setting();
        tokenId = token;
        deviceId = device;
        mydb = new DBHelper(context);
        if (isConnected()) {
            new SearchListJSONParse().execute();
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }


    }

    public boolean addItem(Context context, int menuId, String menuTitle, String imageURL, String Price, String calories, String MenuDetail, String MenuTypeId) {
        String imageURLFull = imageURL;
        items.add(new Item(menuId, menuTitle, imageURLFull, Price, Price, calories, "1", MenuDetail, MenuTypeId));
        return true;
    }

    public boolean clearItem() {
        items.clear();
        return true;
    }

    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view==(FrameLayout)object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View item_view = layoutInflater.inflate(R.layout.slide_show_image, container, false);
        ImageView picture = (ImageView) item_view.findViewById(R.id.picture);
        TextView itemName = (TextView) item_view.findViewById(R.id.itemName);

        final Item item = (Item) getItem(position);

        com.squareup.picasso.Picasso.with(mContext).load(item.drawableId).into(picture);
        itemName.setText(item.itemName);

        container.addView(item_view);

        return item_view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((FrameLayout)object);
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private class Item {
        final int menuId;
        final String itemRate;
        final String itemName;
        String itemPrice;
        final String drawableId;
        String itemCalories;
        String itemQty;
        final String menuDetail, menuTypeId;

        Item(int id, String name, String drawableId, String price, String itemRate, String calories, String itemQty, String menuDetail, String menuTypeId) {
            this.menuId = id;
            this.itemRate = itemRate;
            this.itemName = name;
            this.drawableId = drawableId;
            this.itemPrice = price;
            this.itemCalories = calories;
            this.itemQty = itemQty;
            this.menuDetail = menuDetail;
            this.menuTypeId = menuTypeId;
        }
    }


    private class SearchListJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            // np.com.diningpark.helpers.API_Setting url = new np.com.diningpark.helpers.API_Setting();
            String url = URL.ServerUrl + URL.MenuList;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            // Getting JSON from URL

            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, tokenId);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";

            try {

                JSONArray jsonArray = json.optJSONArray("List");
                clearItem();
                //Iterate the jsonArray and print the info of JSONObjects
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String DisplayOrderNumber = jsonObject.optString("DisplayOrderNumber");
                    String MenuCalories = jsonObject.optString("MenuCalories");
                    String MenuDetail = jsonObject.optString("MenuDetail");
                    String MenuId = jsonObject.optString("MenuId");
                    String MenuImageUrl = jsonObject.optString("MenuImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTName");
                    String rate = jsonObject.optString("Rate ");
                    String MenuTypeId = jsonObject.optString("MenuTypeId ");

                    addItem(mContext, Integer.parseInt(MenuId), MenuTitleName, MenuImageUrl, "130", MenuCalories, "", MenuTypeId);

                }
                notifyDataSetChanged();
                //output.setText(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}