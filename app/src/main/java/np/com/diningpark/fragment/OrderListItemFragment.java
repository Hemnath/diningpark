package np.com.diningpark.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import np.com.diningpark.R;

/**
* Created by Technomax-Server on 1/8/2016.
        */
public class OrderListItemFragment extends Fragment {

    android.view.View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.order_list, container, false);
        return v;
    }
}