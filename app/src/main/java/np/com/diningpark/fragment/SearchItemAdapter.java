package np.com.diningpark.fragment;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import np.com.diningpark.MainActivity;
import np.com.diningpark.R;
import np.com.diningpark.SearchMenuActivity;
import np.com.diningpark.TableDetailActivity;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

public class SearchItemAdapter extends BaseAdapter implements Filterable {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private List<Item> itemsSearch = new ArrayList<Item>();
    private LayoutInflater inflater;
    private String tokenId, deviceId;
    private String tableName, orderMasterId, tableID;

    private TextView btnAdd, rate;
    private TextView Qty, SeqQty;
    private DBHelper mydb;
    private Button increase, decrease, btnSeqDecrs, btnSeqIncrs;
    private API_Setting URL;

    private ProgressDialog progressBar;

    public SearchItemAdapter(Context context, String token, String device, String tName, String orderId, String tableId) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        URL = new API_Setting();
        tokenId = token;
        deviceId = device;
        tableName = tName;
        tableID = tableId;
        orderMasterId = orderId;
        mydb = new DBHelper(context);
        if (isConnected()) {
            new SearchListJSONParse().execute();
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }

    }

    private boolean addItem(Context context, int menuId, String menuTitle, String imageURL, String Price, String calories, String MenuDetail, String MenuTypeId) {
        inflater = LayoutInflater.from(context);
        String imageURLFull = imageURL;
        items.add(new Item(menuId, menuTitle, imageURLFull, Price, Price, calories, "1", mydb.getMaxOldOrderSequence(), MenuDetail, MenuTypeId));
        itemsSearch.add(new Item(menuId, menuTitle, imageURLFull, Price, Price, calories, "1", mydb.getMaxOldOrderSequence(), MenuDetail, MenuTypeId));
        return true;
    }

    private boolean clearItem() {
        items.clear();
        return true;
    }

    private void addAll() {
        items.clear();
        itemsSearch.clear();
        if (isConnected()) {
            new SearchListJSONParse().execute();
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).menuId;
    }


    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name, itemDesc;
        final Item item = (Item) getItem(i);
        if (v == null) {
            v = inflater.inflate(R.layout.search_listview, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.itemName, v.findViewById(R.id.itemName));
            v.setTag(R.id.itemDesc, v.findViewById(R.id.itemDesc));
            v.setTag(R.id.tvRate, v.findViewById(R.id.tvRate));
            v.setTag(R.id.tvQty, v.findViewById(R.id.tvQty));
            v.setTag(R.id.tvSeqQty, v.findViewById(R.id.tvSeqQty));
            v.setTag(R.id.btnAdd, v.findViewById(R.id.btnAdd));
            v.setTag(R.id.btnDecrs, v.findViewById(R.id.btnDecrs));
            v.setTag(R.id.btnIncrs, v.findViewById(R.id.btnIncrs));
            v.setTag(R.id.btnSeqDecrs, v.findViewById(R.id.btnSeqDecrs));
            v.setTag(R.id.btnSeqIncrs, v.findViewById(R.id.btnSeqIncrs));

        }
        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.itemName);
        itemDesc = (TextView) v.getTag(R.id.itemDesc);
        rate = (TextView) v.getTag(R.id.tvRate);
        Qty = (TextView) v.getTag(R.id.tvQty);
        SeqQty = (TextView) v.getTag(R.id.tvSeqQty);
        increase = (Button) v.getTag(R.id.btnIncrs);
        decrease = (Button) v.getTag(R.id.btnDecrs);
        btnSeqIncrs = (Button) v.getTag(R.id.btnSeqIncrs);
        btnSeqDecrs = (Button) v.getTag(R.id.btnSeqDecrs);
        btnAdd = (Button) v.getTag(R.id.btnAdd);

        if (mydb.getOrderStatus() >= 5) {
            btnAdd.setEnabled(false);
        } else {
            btnAdd.setEnabled(true);
        }
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mydb.insertOrderList(item.getId(), item.getItemName(), Float.parseFloat(item.itemRate), Integer.parseInt(item.itemQty), item.getItemCalories(), "", item.ItemSequance, "", mydb.geDeliveryTime(item.ItemSequance), 0, 0);
                ((SearchMenuActivity) mContext).itemAddInfo(item.getItemName());
            }
        });
        increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float fPrice;
                float intCalories;
                int intQty = Integer.parseInt(item.getItemQty());
                intCalories = Float.parseFloat(item.getItemCalories()) / intQty;
                intQty++;
                intCalories = intCalories * intQty;
                fPrice = intQty * Float.parseFloat(item.getItemRate());
                item.itemQty = Integer.toString(intQty);
                item.itemPrice = Float.toString(fPrice);
                item.itemCalories = Float.toString(intCalories);
                Qty.setText(String.valueOf(intQty));
                notifyDataSetChanged();
            }
        });
        decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float fPrice;
                float intCalories;
                int intQty = Integer.parseInt(item.getItemQty());
                if (intQty > 1) {
                    intCalories = Float.parseFloat(item.getItemCalories()) / intQty;
                    intQty--;
                    intCalories = intCalories * intQty;
                    fPrice = intQty * Float.parseFloat(item.getItemRate());
                    item.itemQty = Integer.toString(intQty);
                    item.itemPrice = Float.toString(fPrice);
                    item.itemCalories = Float.toString(intCalories);
                    Qty.setText(String.valueOf(intQty));
                    notifyDataSetChanged();
                }
            }
        });

        btnSeqIncrs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intSeq = item.ItemSequance;
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                item.ItemSequance = intSeq;
                SeqQty.setText(String.valueOf(intSeq));
                notifyDataSetChanged();
            }
        });
        btnSeqDecrs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int intSeq = item.ItemSequance;
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                item.ItemSequance = intSeq;
                SeqQty.setText(String.valueOf(intSeq));
                notifyDataSetChanged();
            }
        });

        String imagePathCon = item.drawableId;
        imagePathCon = imagePathCon.replaceAll(" ", "%20");
        com.squareup.picasso.Picasso.with(mContext).load(imagePathCon).into(picture);

        name.setText(item.itemName);
        String desc = item.menuDetail;
        if (desc.length() > 100)
            desc = desc.substring(0, 100) + "......";
        itemDesc.setText(desc);
        Qty.setText(item.itemQty);
        SeqQty.setText(String.valueOf(item.ItemSequance));
        rate.setText(item.getItemPrice());
        return v;
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        clearItem();
        if (charText.length() == 0) {
            addAll();
        } else {
            for (Item wp : itemsSearch) {
                String strMenuId = Integer.toString(wp.getItemId());
                if (wp.getItemName().toLowerCase(Locale.getDefault()).contains(charText) || strMenuId.toLowerCase(Locale.getDefault()).contains(charText)) {
                    items.add(new Item(wp.getId(), wp.getItemName(), wp.getDrawableId(), wp.getItemPrice(), wp.getItemRate(), wp.getItemCalories(), wp.getItemQty(), wp.getItemSequance(), wp.getItemMenuDetail(), wp.getItemMenuTypeId()));
                }
            }
        }
        notifyDataSetChanged();
    }

    private class Item {
        final int menuId;
        final String itemRate;
        final String itemName;
        String itemPrice;
        final String drawableId;
        String itemCalories;
        String itemQty;
        int ItemSequance;
        final String menuDetail, menuTypeId;

        Item(int id, String name, String drawableId, String price, String itemRate, String calories, String itemQty, int itemSequance, String menuDetail, String menuTypeId) {
            this.menuId = id;
            this.itemRate = itemRate;
            this.itemName = name;
            this.drawableId = drawableId;
            this.itemPrice = price;
            this.itemCalories = calories;
            this.itemQty = itemQty;
            this.menuDetail = menuDetail;
            this.menuTypeId = menuTypeId;
            this.ItemSequance = itemSequance;
        }

        public int getItemId() {
            return this.menuId;
        }

        public int getItemSequance() {
            return this.ItemSequance;
        }

        private String getItemName() {
            return this.itemName;
        }

        private String getItemPrice() {
            return this.itemPrice;
        }

        private String getItemRate() {
            return this.itemRate;
        }

        private String getItemMenuDetail() {
            return this.menuDetail;
        }

        private String getItemMenuTypeId() {
            return this.menuTypeId;
        }

        private String getItemQty() {
            return this.itemQty;
        }

        private String getDrawableId() {
            return this.drawableId;
        }

        private String getItemCalories() {
            return this.itemCalories;
        }

        public int getId() {
            return this.menuId;
        }
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("DONE", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    private class SearchListJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar = new ProgressDialog(mContext);
            progressBar.setMessage("Downloading...");
            progressBar.show();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            // np.com.diningpark.helpers.API_Setting url = new np.com.diningpark.helpers.API_Setting();
            String url = URL.ServerUrl + URL.MenuList;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            // Getting JSON from URL

            org.json.JSONObject json = jParser.getJSONFromUrl(url, deviceId, tokenId);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
            try {

                JSONArray jsonArray = json.optJSONArray("List");
                clearItem();
                //Iterate the jsonArray and print the info of JSONObjects
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String DisplayOrderNumber = jsonObject.optString("DisplayOrderNumber");
                    String MenuCalories = jsonObject.optString("MenuCalories");
                    String MenuDetail = jsonObject.optString("MenuIngredient");
                    String MenuId = jsonObject.optString("MenuId");
                    String MenuImageUrl = jsonObject.optString("MenuImageUrl");
                    String MenuTitleName = jsonObject.optString("MenuTName");
                    String rate = jsonObject.optString("Rate");
                    String MenuTypeId = jsonObject.optString("MenuTypeId ");

                    Float fRate = Float.parseFloat(rate);
                    String strRate = String.format("%.0f", fRate);

                    addItem(mContext, Integer.parseInt(MenuId), MenuTitleName, MenuImageUrl, strRate, MenuCalories, MenuDetail, MenuTypeId);

                }
                notifyDataSetChanged();
                //output.setText(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
