package np.com.diningpark.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.EditOrderListActivity;
import np.com.diningpark.R;
import np.com.diningpark.WelcomeActivity;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;

/**
 * Created by Hemnath on 3/4/2016.
 */
public class EditOrderListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Object> items = new ArrayList<Object>();
    private ArrayList<Integer> hiddenPositions = new ArrayList<>();
    protected LayoutInflater inflater;
    View v;
    private DBHelper mydb;
    private String tokenId, strdeviceId, url;
    private int tableId;
    private API_Setting URL;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_DIVIDER = 1;

    public EditOrderListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        SharedPreferences sharedpreferences = mContext.getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        strdeviceId = sharedpreferences.getString("DeviceId", null);
        tokenId = sharedpreferences.getString("TokenId", null);
        tableId = sharedpreferences.getInt("tableId", 1);
        URL = new API_Setting();
        mydb = new DBHelper(context);
    }

    public int setOrderList(Context context) {
        inflater = LayoutInflater.from(context);
        ArrayList<Integer> array_list;
        int returnValue = 1;
        items.clear();
        int cnt = 0;
        for (int sq = 1; sq <= mydb.getMaxOrderSequence(); sq++) {
            items.add(cnt, " Order Sequence " + sq + " ");
            array_list = mydb.getAllOrderList1(sq);
            int count = array_list.size();
            for (int i = 0; i < count; i++) {
                cnt++;
                Cursor rs = mydb.getData(array_list.get(i));
                rs.moveToFirst();
                String strId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ID));
                String strItemId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ITEMID));
                String strName = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_NAME));
                float strRate = rs.getFloat(rs.getColumnIndex(DBHelper.COLUMN_PRICE));
                String strQty = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_QTY));
                int StrCalories = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_CALORIES));
                int strOrderStatus = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_ORDERSTATUS));
                String orderDetailId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OREDERDETAILID));
                int intOrderSequence = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_OrderSequence));
                int orderDeliveryTime = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_DeliveryTime));
                int takeAway = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_TakeAway));
                String strOrderMessage = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OrderMessage));
                float totalPrice = strRate * Integer.parseInt(strQty);
                items.add(cnt, new Item(strId, strItemId, strName, Float.toString(totalPrice), Float.toString(strRate), Integer.toString(StrCalories), strQty, orderDetailId, takeAway, intOrderSequence, strOrderMessage, orderDeliveryTime, strOrderStatus));
                rs.close();
            }
            cnt++;
        }
        return returnValue;
    }

    public boolean updateOrderList() {
        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, item.orderDetailId, item.orderSequence, item.orderMessage, mydb.geDeliveryTime(item.orderSequence), item.takeAway, item.orderStatus);
                for (Integer hiddenIndex : hiddenPositions) {
                    if (hiddenIndex == i) {
                        mydb.deleteOrderList(Integer.parseInt(item.id));
                    }
                }
            }
        }
        return true;
    }

    public void addhiddenPositions(int finalI) {
        hiddenPositions.add(finalI);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Item) {
            return TYPE_ITEM;
        }
        return TYPE_DIVIDER;
    }

    @Override
    public boolean isEnabled(int position) {
        return (getItemViewType(position) == TYPE_ITEM);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        v = view;
        final ViewHolder vh;
        int type = getItemViewType(i);
        if (v == null) {
            v = inflater.inflate(R.layout.edit_order_list, viewGroup, false);
            vh = new ViewHolder(v.findViewById(R.id.spinner), v.findViewById(R.id.tvName), v.findViewById(R.id.edtMessage), v.findViewById(R.id.item_id), v.findViewById(R.id.tvPrice), v.findViewById(R.id.tvRate), v.findViewById(R.id.tvQty), v.findViewById(R.id.tvcalories), v.findViewById(R.id.listDelete), v.findViewById(R.id.btnIncrs), v.findViewById(R.id.btnDecrs), v.findViewById(R.id.tvStatus), v.findViewById(R.id.cbTakeAway), v.findViewById(R.id.tvSectionName), v.findViewById(R.id.sectionHeader), v.findViewById(R.id.sectionItem), v.findViewById(R.id.dragBtn));
            v.setTag(vh);
        } else {
            vh = (ViewHolder) v.getTag();
        }
        final ViewHolder finalVh = vh;
        switch (type) {
            case TYPE_DIVIDER:
                vh.sectionHeader.setVisibility(View.VISIBLE);
                vh.sectionItem.setVisibility(View.GONE);

                final String titleString = (String) getItem(i);
                vh.tvSectionName.setText(titleString);

                ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mContext,
                        R.array.DeliveryTime, android.R.layout.simple_dropdown_item_1line);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                vh.spinner.setAdapter(adapter);
                int deliveryTime = mydb.geDeliveryTime(Integer.parseInt(titleString.replace(" Order Sequence ", "").replace(" ", "")));

                if (deliveryTime == 0)
                    vh.spinner.setSelection(0);
                else
                    vh.spinner.setSelection(deliveryTime / 5);

                vh.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mydb.setDeliveryTime(Integer.parseInt(titleString.replace(" Order Sequence ", "").replace(" ", "")), Integer.parseInt(finalVh.spinner.getSelectedItem().toString().replace(" Min", "")));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

                break;
            case TYPE_ITEM:
                final Item item = (Item) getItem(i);
                vh.sectionHeader.setVisibility(View.GONE);
                vh.sectionItem.setVisibility(View.VISIBLE);

                final int finalI = i;

                vh.message.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        item.orderMessage = s.toString();
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        item.orderMessage = s.toString();
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });


                vh.listDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        hiddenPositions.add(finalI);
                        notifyDataSetChanged();
                    }
                });
                vh.btnIncrs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        float intCalories, fPrice;
                        int intQty = Integer.parseInt(item.qty);
                        intCalories = Float.parseFloat(item.calories) / intQty;
                        intQty++;
                        intCalories = intCalories * intQty;
                        fPrice = intQty * Float.parseFloat(item.rate);
                        item.qty = Integer.toString(intQty);
                        item.price = Float.toString(fPrice);
                        item.calories = Float.toString(intCalories);
                        notifyDataSetChanged();
                    }
                });
                vh.btnDecrs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        float intCalories, fPrice;
                        int intQty = Integer.parseInt(item.qty);
                        if (intQty > 1) {
                            intCalories = Float.parseFloat(item.calories) / intQty;
                            intQty--;
                            intCalories = intCalories * intQty;
                            fPrice = intQty * Float.parseFloat(item.rate);
                            item.qty = Integer.toString(intQty);
                            item.price = Float.toString(fPrice);
                            item.calories = Float.toString(intCalories);
                            notifyDataSetChanged();
                        }
                    }
                });

                vh.cbTakeAway.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked)
                            item.takeAway = 1;
                        else
                            item.takeAway = 0;
                    }
                });


                int color;
                String strStatus;
                strStatus = "New Order";
                color = Color.GREEN;
                vh.listDelete.setVisibility(View.VISIBLE);
                boolean btnEnbable = false;
                if (item.orderStatus == 0) {
                    color = Color.parseColor("#4CAF5C");
                    strStatus = "New Order";
                    btnEnbable = true;
                } else if (item.orderStatus == 1) {
                    color = Color.MAGENTA;
                    strStatus = "Place Order";
                    btnEnbable = false;
                } else if (item.orderStatus == 2) {
                    color = Color.MAGENTA;
                    strStatus = "In Progress";
                    btnEnbable = false;
                } else if (item.orderStatus == 3) {
                    color = Color.GREEN;
                    strStatus = "Weating for Delivery";
                    btnEnbable = false;
                } else if (item.orderStatus == 4) {
                    color = Color.GREEN;
                    strStatus = "Delivered";
                    btnEnbable = false;
                } else if (item.orderStatus == 5) {
                    color = Color.GRAY;
                    strStatus = "Completed";
                    btnEnbable = false;
                } else if (item.orderStatus == 6) {
                    color = Color.RED;
                    strStatus = "Canceled";
                    btnEnbable = false;
                } else if (item.orderStatus == 7) {
                    color = Color.DKGRAY;
                    strStatus = "Accepted";
                    btnEnbable = false;
                } else if (item.orderStatus == 8) {
                    color = Color.RED;
                    strStatus = "Rejected";
                    btnEnbable = false;
                } else if (item.orderStatus == 9) {
                    color = Color.BLUE;
                    strStatus = "Invoive Requested";
                    btnEnbable = false;
                }

                if (btnEnbable) {
                    vh.dragBtn.setVisibility(View.VISIBLE);
                    vh.btnIncrs.setVisibility(View.VISIBLE);
                    vh.btnDecrs.setVisibility(View.VISIBLE);
                    vh.listDelete.setVisibility(View.VISIBLE);
                    vh.cbTakeAway.setEnabled(true);
                    vh.message.setEnabled(true);
                } else {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    vh.btnIncrs.setVisibility(View.INVISIBLE);
                    vh.btnDecrs.setVisibility(View.INVISIBLE);
                    vh.listDelete.setVisibility(View.INVISIBLE);
                    vh.cbTakeAway.setEnabled(false);
                    vh.message.setEnabled(false);
                }

                vh.tvStatus.setText(strStatus);
                vh.name.setTextColor(color);
                vh.price.setTextColor(color);
                vh.tvcalories.setTextColor(color);
                vh.qty.setTextColor(color);
                vh.rate.setTextColor(color);
                vh.tvStatus.setTextColor(color);

                vh.name.setText(item.name);
                vh.message.setText(item.orderMessage);

                Float fRate = Float.parseFloat(item.rate);
                String strRate = String.format("%.0f", fRate);
                Float fPrice = Float.parseFloat(item.price);
                String strPrice = String.format("%.0f", fPrice);
                vh.rate.setText(strRate);
                vh.price.setText(strPrice);
                vh.tvcalories.setText(item.calories);
                vh.qty.setText(item.qty);
                vh.id.setText(item.id);

                if (item.takeAway == 1)
                    vh.cbTakeAway.setChecked(true);
                else
                    vh.cbTakeAway.setChecked(false);

                final int sq = item.orderSequence;
                vh.dragBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((EditOrderListActivity) mContext).orderSequence(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, item.orderDetailId, sq, item.orderMessage, item.deliveryTime, item.takeAway, item.orderStatus);
                    }
                });
                break;
        }
        for (Integer hiddenIndex : hiddenPositions) {
            if (hiddenIndex == i) {
                v.setVisibility(View.INVISIBLE);
            }
        }
        return v;
    }

    private static class ViewHolder {

        final public Spinner spinner;
        final public ImageView dragBtn;
        final public EditText message;
        final public TextView name;
        final public TextView id;
        final public TextView price;
        final public TextView rate;
        final public TextView qty;
        final public TextView tvcalories;
        final public Button listDelete;
        final public Button btnIncrs;
        final public Button btnDecrs;
        final public CheckBox cbTakeAway;
        final public TextView tvStatus;
        final public TextView tvSectionName;
        final public LinearLayout sectionHeader;
        final public LinearLayout sectionItem;

        public ViewHolder(View v1, View v2, View v2_1, View v3, View v4, View v5, View v6, View v7, View v8, View v9, View v10, View v11, View cb1, View v12, View v13, View v14, View v15) {
            spinner = (Spinner) v1;
            name = (TextView) v2;
            message = (EditText) v2_1;
            id = (TextView) v3;
            price = (TextView) v4;
            rate = (TextView) v5;
            qty = (TextView) v6;
            tvcalories = (TextView) v7;
            listDelete = (Button) v8;
            btnIncrs = (Button) v9;
            btnDecrs = (Button) v10;
            tvStatus = (TextView) v11;
            cbTakeAway = (CheckBox) cb1;
            tvSectionName = (TextView) v12;
            sectionHeader = (LinearLayout) v13;
            sectionItem = (LinearLayout) v14;
            dragBtn = (ImageView) v15;
        }
    }

    public class Item {
        final String id;
        final String itemId;
        final String name;
        String price;
        final String rate;
        String calories;
        String qty;
        final String orderDetailId;
        int takeAway;
        final int orderStatus;
        final int orderSequence;
        String orderMessage;
        final int deliveryTime;

        Item(String id, String itemId, String name, String price, String rate, String calories, String qty, String OrderItemsId, int takeAway, int orderSequence, String orderMessage, int deliveryTime, int status) {
            this.id = id;
            this.itemId = itemId;
            this.name = name;
            this.price = price;
            this.rate = rate;
            this.calories = calories;
            this.qty = qty;
            this.takeAway = takeAway;
            this.orderStatus = status;
            this.orderDetailId = OrderItemsId;
            this.orderSequence = orderSequence;
            this.orderMessage = orderMessage;
            this.deliveryTime = deliveryTime;
        }
    }

    public String DeleteOrderItemPOST(String url) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";


            JSONObject jsonObject = new JSONObject();

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();


            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class DeleteOrderItem extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return DeleteOrderItemPOST(urls[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Deleted") && MsgType.equalsIgnoreCase("Success")) {

                    Toast.makeText(mContext, "Remove Item",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Error In Remove Item",
                            Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }

}