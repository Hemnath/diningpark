package np.com.diningpark.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;
import np.com.diningpark.TableDetailActivity;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

/**
 * Created by ik890 on 3/23/2016.
 */
public class TableOrderListAdapter extends BaseAdapter {
    private Context mContext;
    private List<Object> items = new ArrayList<Object>();
    private LayoutInflater inflater;
    View v;
    private DBHelper mydb;

    private String tokenId, strdeviceId, url;
    private int tableId;

    private static String orderMasterId;
    private API_Setting URL;
    private String orderMsg;
    String custName, custMobile, custPerson;

    private static final int TYPE_ITEM = 0;
    private static final int TYPE_DIVIDER = 1;


    public TableOrderListAdapter(Context context, String orderId, String deviceId, String token) {
        inflater = LayoutInflater.from(context);
        mContext = context;
        mydb = new DBHelper(context);
        strdeviceId = deviceId;
        tokenId = token;
        URL = new API_Setting();
        orderMasterId = orderId;
        orderMsg = "New Order";
    }

    public int setOrderList(Context context) {
        inflater = LayoutInflater.from(context);
        ArrayList<Integer> array_list;
        int returnValue = 1;
        items.clear();
        int cnt = 0;
        for (int sq = 1; sq <= mydb.getMaxOrderSequence(); sq++) {
            items.add(cnt, " Order Sequence " + sq + " ");
            array_list = mydb.getAllOrderList1(sq);
            int count = array_list.size();
            for (int i = 0; i < count; i++) {
                cnt++;
                Cursor rs = mydb.getData(array_list.get(i));
                rs.moveToFirst();
                String strId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ID));
                String strItemId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_ITEMID));
                String strName = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_NAME));
                float strRate = rs.getFloat(rs.getColumnIndex(DBHelper.COLUMN_PRICE));
                String strQty = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_QTY));
                int StrCalories = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_CALORIES));
                int strOrderStatus = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_ORDERSTATUS));
                String orderDetailId = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OREDERDETAILID));
                int intOrderSequence = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_OrderSequence));
                int orderDeliveryTime = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_DeliveryTime));
                int takeAway = rs.getInt(rs.getColumnIndex(DBHelper.COLUMN_TakeAway));
                String strOrderMessage = rs.getString(rs.getColumnIndex(DBHelper.COLUMN_OrderMessage));
                float totalPrice = strRate * Integer.parseInt(strQty);
                items.add(cnt, new Item(strId, strItemId, strName, Float.toString(totalPrice), Float.toString(strRate), Integer.toString(StrCalories), strQty, orderDetailId, intOrderSequence, strOrderMessage, orderDeliveryTime, strOrderStatus, takeAway));
                rs.close();
            }
            cnt++;
        }
        return returnValue;
    }

    public String getOrderMasterId() {
        return orderMasterId;
    }

    public void setOldOrder() {
        url = URL.ServerUrl + URL.OrderDetail + orderMasterId;

        if (isConnected()) {
            new GetOrderJSONParse().execute(url);
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }


    //TODO : SEQUENCE NUMBER
    public boolean insertOrderList(String item_id, String menuName, String price, String Qty, String intCalories, String orderDetailId, int sequenceNumber, String message, int deliveryTime, int takeAway, String orderStatusId) {
        float fp = Float.parseFloat(price);
        mydb.insertOrderList(Integer.parseInt(item_id), menuName, fp, Integer.parseInt(Qty), intCalories, orderDetailId, sequenceNumber, message, deliveryTime, takeAway, Integer.parseInt(orderStatusId));
        setOrderList(mContext);
        notifyDataSetChanged();

        return true;
    }

    public void pushNewOrderList(String token, int table, String deviceId, String cMsg, String cName, String cMobile, String cPerson) {
        tokenId = token;
        tableId = table;
        strdeviceId = deviceId;
        orderMsg = cMsg;
        custName = cName;
        custMobile = cMobile;
        custPerson = cPerson;

        if (this.orderMasterId.equals("0") || this.orderMasterId.equals("")) {
            url = URL.ServerUrl + URL.PostOrdere;
            if (isConnected()) {
                new PushOrderList().execute(url);
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        } else {

            if (isConnected()) {
                placeNewOrderItem();
            } else {
                showAlertDialog(mContext, "No Internet Connection",
                        "You don't have internet connection.", false);
            }

        }

    }

    public void placeNewOrderItem() {
        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                if (item.orderStatus == 0) {
                    url = URL.ServerUrl + URL.AddNewOrderItem;

                    if (isConnected()) {
                        Boolean blnTakeAway=false;
                        if (item.takeAway == 1)
                            blnTakeAway=true;
                        new PushNewOrderItem(item.itemId, item.qty, item.orderSequence, item.orderMessage, item.deliveryTime,blnTakeAway).execute(url);
                    } else {
                        showAlertDialog(mContext, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                }
            }
            ((TableDetailActivity) mContext).reset();
        }
    }

    //TODO : SEQUENCE NUMBER
    public void updateOrderStatus(String mId, String OrderStatusId, String OrderDetailId) {

        for (int i = 0; i < items.size(); i++) {
            if (getItem(i) instanceof Item) {
                final Item item = (Item) getItem(i);
                if (item.itemId.equalsIgnoreCase(mId) && item.orderDetailId.equalsIgnoreCase(OrderDetailId)) {

                    mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, OrderDetailId, item.orderSequence, item.orderMessage, item.deliveryTime, item.takeAway, Integer.parseInt(OrderStatusId));

                } else if (item.itemId.equalsIgnoreCase(mId) && item.orderDetailId.equalsIgnoreCase("")) {
                    mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, OrderDetailId, item.orderSequence, item.orderMessage, item.deliveryTime, item.takeAway, Integer.parseInt(OrderStatusId));
                }
            }
        }
    }

    public void OrderStatus() {
        if (isConnected()) {
            new OrderDetailsJSONParse2().execute("");
        } else {
            showAlertDialog(mContext, "No Internet Connection",
                    "You don't have internet connection.", false);
        }
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }


    public boolean orderSequence(final Item item, int sq) {
        final boolean[] done = {false};
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final View dialogView = inflater.inflate(R.layout.order_sequence, null);
        dialogBuilder.setView(dialogView);

        final TextView txtDiagSeqNo = (TextView) dialogView.findViewById(R.id.diagSeqNo);
        txtDiagSeqNo.setText(String.valueOf(sq));

        Button btnDecrsSeq = (Button) dialogView.findViewById(R.id.btnDecrsSeq);
        Button btnIncrsSeq = (Button) dialogView.findViewById(R.id.btnIncrsSeq);

        btnIncrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq <= mydb.getMaxOrderSequence())
                    intSeq++;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });
        btnDecrsSeq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                if (intSeq > mydb.getMaxOldOrderSequence())
                    intSeq--;
                txtDiagSeqNo.setText(String.valueOf(intSeq));
            }
        });

        dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                done[0] = false;
            }

        }).setPositiveButton("Done", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String tempSeq = txtDiagSeqNo.getText().toString();
                int intSeq = Integer.parseInt(tempSeq);
                mydb.updateOrderList(Integer.parseInt(item.id), item.name, Float.parseFloat(item.rate), Integer.parseInt(item.qty), item.calories, item.orderDetailId, intSeq, item.orderMessage, mydb.geDeliveryTime(intSeq), item.takeAway, item.orderStatus);
                setOrderList(mContext);
                notifyDataSetChanged();
                done[0] = true;
            }
        });


        AlertDialog b = dialogBuilder.create();
        b.show();
        return done[0];
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        if (getItem(position) instanceof Item) {
            return TYPE_ITEM;
        }
        return TYPE_DIVIDER;
    }

    @Override
    public boolean isEnabled(int position) {
        return (getItemViewType(position) == TYPE_ITEM);
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        v = view;
        ViewHolder vh;
        int type = getItemViewType(i);
        if (v == null) {
            v = inflater.inflate(R.layout.order_list_item, viewGroup, false);
            vh = new ViewHolder(v.findViewById(R.id.dragBtn), v.findViewById(R.id.tvName), v.findViewById(R.id.item_id), v.findViewById(R.id.tvPrice), v.findViewById(R.id.tvRate), v.findViewById(R.id.tvQty), v.findViewById(R.id.tvSectionName), v.findViewById(R.id.sectionHeader), v.findViewById(R.id.sectionItem));
            v.setTag(vh);
        } else {
            vh = (ViewHolder) v.getTag();
        }
        switch (type) {
            case TYPE_DIVIDER:
                int width1 = 0;
                int height1 = 0;
                LinearLayout.LayoutParams parms1 = new LinearLayout.LayoutParams(width1, height1);
                vh.sectionItem.setLayoutParams(parms1);
                String titleString = (String) getItem(i);
                vh.tvSectionName.setText(titleString);
                break;
            case TYPE_ITEM:
                final Item item = (Item) getItem(i);
                int width = 0;
                int height = 0;
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                vh.sectionHeader.setLayoutParams(parms);
                int color;
                color = Color.GREEN;
                if (item.orderStatus == 0) {
                    vh.dragBtn.setVisibility(View.VISIBLE);
                    color = Color.parseColor("#4CAF5C");
                } else if (item.orderStatus == 1) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.MAGENTA;
                } else if (item.orderStatus == 2) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.MAGENTA;
                } else if (item.orderStatus == 3) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GREEN;
                } else if (item.orderStatus == 4) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GREEN;
                } else if (item.orderStatus == 5) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.GRAY;
                } else if (item.orderStatus == 6) {
                    vh.dragBtn.setVisibility(View.INVISIBLE);
                    color = Color.RED;
                }
                vh.name.setTextColor(color);
                vh.price.setTextColor(color);
                vh.rate.setTextColor(color);
                vh.qty.setTextColor(color);

                vh.name.setText(item.name);
                Float fRate = Float.parseFloat(item.rate);
                String strRate = String.format("%.0f", fRate);
                Float fPrice = Float.parseFloat(item.price);
                String strPrice = String.format("%.0f", fPrice);
                vh.rate.setText(strRate);
                vh.price.setText(strPrice);
                vh.qty.setText(item.qty);
                vh.id.setText(item.id);

                final int sq = item.orderSequence;
                vh.dragBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderSequence(item, sq);
                    }
                });
                break;
        }
        return v;
    }

    public static class ViewHolder {
        public ImageView dragBtn;
        public TextView name;
        public TextView id;
        public TextView price;
        public TextView rate;
        public TextView qty;
        public TextView tvSectionName;
        public LinearLayout sectionHeader;
        public LinearLayout sectionItem;

        public ViewHolder(View v1, View v2, View v3, View v4, View v5, View v6, View v7, View v8, View v9) {
            dragBtn = (ImageView) v1;
            name = (TextView) v2;
            id = (TextView) v3;
            price = (TextView) v4;
            rate = (TextView) v5;
            qty = (TextView) v6;
            tvSectionName = (TextView) v7;
            sectionHeader = (LinearLayout) v8;
            sectionItem = (LinearLayout) v9;
        }
    }


    private class Item {
        final String id;
        final String itemId;
        final String name;
        final String price;
        final String rate;
        final String calories;
        final String qty;
        final String orderDetailId;
        final int orderStatus;
        final int takeAway;
        final int orderSequence;
        final String orderMessage;
        final int deliveryTime;

        Item(String id, String itemId, String name, String price, String rate, String calories, String qty, String OrderItemsId, int orderSequence, String orderMessage, int deliveryTime, int status, int takeAway) {
            this.id = id;
            this.itemId = itemId;
            this.name = name;
            this.price = price;
            this.rate = rate;
            this.calories = calories;
            this.qty = qty;
            this.orderStatus = status;
            this.orderDetailId = OrderItemsId;
            this.orderSequence = orderSequence;
            this.orderMessage = orderMessage;
            this.deliveryTime = deliveryTime;
            this.takeAway = takeAway;
        }
    }

    public String POST(String url) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";

            JSONArray jsonArray = new JSONArray();
            JSONObject jsonObject = new JSONObject();

            for (int i = 0; i < items.size(); i++) {
                JSONObject jsonList = new JSONObject();
                if (getItem(i) instanceof Item) {
                    final Item item = (Item) getItem(i);
                    jsonList.put("MenuId", Integer.parseInt(item.itemId));
                    jsonList.put("Quantity", Integer.parseInt(item.qty));
                    jsonList.put("DeliveryTime", item.deliveryTime);
                    jsonList.put("Message", item.orderMessage);
                    jsonList.put("SequenceNumber", item.orderSequence);
                    if (item.takeAway == 1)
                        jsonList.put("TakeAway", true);
                    else
                        jsonList.put("TakeAway", false);
                    jsonArray.put(jsonList);

                }
            }
            jsonObject.put("ResTableId", tableId);
            jsonObject.put("CustomerName", custName);
            jsonObject.put("MobileNumber", custMobile);
            jsonObject.put("Persons", custPerson);
            jsonObject.put("CustomerMessage", orderMsg);

            jsonObject.put("ItemList", jsonArray);

            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            System.out.println("JSON File = " + json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushOrderList extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            return POST(urls[0]);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    orderMasterId = json.optString("OrderMasterId");
                    String InvoiceNo = json.optString("InvoiceNo");

                    mydb.insertOrderDetails(orderMasterId, InvoiceNo, 1);
                    ((TableDetailActivity) mContext).setOrderMasterId(orderMasterId);
                    ((TableDetailActivity) mContext).reset();
                    OrderStatus();

                    Toast.makeText(mContext, "Order Place",
                            Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(mContext, "Error In Order Place",
                            Toast.LENGTH_SHORT).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class GetOrderJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.GetOrder + orderMasterId;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, tokenId);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("ItemList");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String MenuId = jsonObject.optString("MenuId");
                    String MenuName = jsonObject.optString("MenuName");
                    String OrderStatusId = jsonObject.optString("OrderStatusId");
                    String OrderDetailId = jsonObject.optString("OrderItemsId");
                    String Rate = jsonObject.optString("Rate");
                    String Calories = jsonObject.optString("Calories");
                    String Quantity = jsonObject.optString("Quantity");
                    int DeliveryTime = jsonObject.optInt("DeliveryTime");
                    boolean takeAway = jsonObject.optBoolean("TakeAway");
                    int SequenceNumber = jsonObject.optInt("SequenceNumber");
                    String Message = jsonObject.optString("Message");
                    int intTakeAway = 0;
                    if (takeAway) {
                        intTakeAway = 1;
                    }
                    insertOrderList(MenuId, MenuName, Rate, Quantity, Calories, OrderDetailId, SequenceNumber, Message, DeliveryTime, intTakeAway, OrderStatusId);
                }

                orderMasterId = json.optString("OrderMasterId");
                String InvoiceNo = json.optString("InvoiceNo").replace("null", "0");
                String OrderStatusId = json.optString("OrderStatusId");
                mydb.deleteAllOrderDetails();
                System.out.println("OrderStatusId == " + OrderStatusId);

                mydb.insertOrderDetails(orderMasterId, InvoiceNo, Integer.parseInt(OrderStatusId));

                System.out.println("OrderStatusId == " + mydb.getOrderStatus());

                setOrderList(mContext);
                notifyDataSetChanged();
                ((TableDetailActivity) mContext).setTotalInfo();
                ((TableDetailActivity) mContext).refresh();
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String NewORDERPOST(String url, String newOrderMenuId, String newOrderMenuQty, int SequenceNumber, String OrderMessage, int DeliveryTime, boolean takeAway) {

        InputStream inputStream = null;
        String result = "";

        try {
            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";

            JSONObject jsonObject = new JSONObject();

            jsonObject.put("MenuId", newOrderMenuId);
            jsonObject.put("OrderMasterId", orderMasterId);
            jsonObject.put("Quantity", newOrderMenuQty);
            jsonObject.put("DeliveryTime", DeliveryTime);
            jsonObject.put("Message", OrderMessage);
            jsonObject.put("SequenceNumber", SequenceNumber);
            jsonObject.put("TakeAway", takeAway);

            json = jsonObject.toString();
            StringEntity se = new StringEntity(json);

            httpPost.setEntity(se);

            httpPost.setHeader("Token", tokenId);
            httpPost.setHeader("DeviceId", strdeviceId);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = httpclient.execute(httpPost);

            inputStream = httpResponse.getEntity().getContent();

            if (inputStream != null)
                result = convertInputStreamToString(inputStream);
            return result;

        } catch (Exception e) {
        }

        return result;

    }

    private class PushNewOrderItem extends AsyncTask<String, Void, String> {
        String OrderMenuId;
        String OrderMenuQty;
        int SequenceNumber;
        String OrderMessage;
        int DeliveryTime;
        boolean takeAway;

        public PushNewOrderItem(String OrderMenuId1, String OrderMenuQty1, int sequenceNumber, String orderMessage, int deliveryTime, boolean takeAway) {
            OrderMenuId = OrderMenuId1;
            OrderMenuQty = OrderMenuQty1;
            SequenceNumber = sequenceNumber;
            DeliveryTime = deliveryTime;
            OrderMessage = orderMessage;
            takeAway = takeAway;
        }

        @Override
        protected String doInBackground(String... urls) {
            return NewORDERPOST(urls[0], OrderMenuId, OrderMenuQty, SequenceNumber, OrderMessage, DeliveryTime, takeAway);

        }

        @Override
        protected void onPostExecute(String result) {
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    String strMenuId = json.optString("MenuId");
                    orderMasterId = json.optString("OrderMasterId");
                    String OrderItemsId = json.optString("OrderItemsId");
                    updateOrderStatus(strMenuId, "1", OrderItemsId);
                    ((TableDetailActivity) mContext).setOrderMasterId(orderMasterId);
                    ((TableDetailActivity) mContext).reset();
                    Toast.makeText(mContext, "New Order Item Place",
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "Error In Order Place",
                            Toast.LENGTH_SHORT).show();
                }
                notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private class OrderDetailsJSONParse2 extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {

            orderMasterId = mydb.getOrderMasterId();
            url = URL.ServerUrl + URL.OrderDetail + orderMasterId;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, tokenId);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);


                    String MenuId = jsonObject.optString("MenuId");
                    String OrderStatusId = jsonObject.optString("OrderStatusId");
                    String OfflineOrderDetailId = jsonObject.optString("OrderItemsId");

                    updateOrderStatus(MenuId, OrderStatusId, OfflineOrderDetailId);
                    notifyDataSetChanged();
                }
                setOrderList(mContext);
                notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }
            notifyDataSetChanged();

        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }
}