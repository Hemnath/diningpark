package np.com.diningpark.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;
import np.com.diningpark.WelcomeActivity;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.JSONParserCustom;

/**
 * Created by Hermnath on 3/21/2016.
 */

public class WaiterTableAdapter  extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;
    Context mainContext;
    String url, strdeviceId, token;
    API_Setting URL;
    SharedPreferences.Editor editor;
    Intent myIntent;
    SharedPreferences sharedpreferences;

    public WaiterTableAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mainContext = context;
        URL = new API_Setting();

        sharedpreferences = mainContext.getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        strdeviceId = sharedpreferences.getString("DeviceId", null);
        token = sharedpreferences.getString("TokenId", null);

    }
    public void checkTableStatus(){
        url = URL.ServerUrl + URL.TableStatus;
        new WaiterTableJSONParse().execute(url);
    }

    public void addTable(String ResTableName, String MasterStatusName, String ResTableId,String isOccupied,String masterStatusId,String masterStatusName,String offlineOrderMasterId,String resRoomName) {

        items.add(new Item(ResTableName, MasterStatusName, isOccupied, masterStatusId, masterStatusName, offlineOrderMasterId, resRoomName, Integer.parseInt(ResTableId)));

    }
    public String getTableNo(int i) {
        return items.get(i).tableNo;
    }

    public String getOfflineOrderMasterId(int i) {
        return items.get(i).OfflineOrderMasterId;
    }


    public int getTableId(int i) {
        return items.get(i).tableId;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).tableId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        TextView status;
        TextView tableNo;
        CardView cardView;

        if (v == null) {
            v = inflater.inflate(R.layout.waiter_table_adapter, viewGroup, false);
            v.setTag(R.id.cardView, v.findViewById(R.id.cardView));
            v.setTag(R.id.tableNo, v.findViewById(R.id.tableNo));
            v.setTag(R.id.status, v.findViewById(R.id.status));
        }

        status = (TextView) v.getTag(R.id.status);
        cardView = (CardView) v.getTag(R.id.cardView);
        tableNo = (TextView) v.getTag(R.id.tableNo);

        Item item = (Item) getItem(i);

        int color;
        color = Color.GRAY;

        if (item.MasterStatusId.equalsIgnoreCase("0")) {
            color =Color.LTGRAY;
        } else if (item.MasterStatusId.equalsIgnoreCase("1")) {
            color = Color.GREEN;
        } else if (item.MasterStatusId.equalsIgnoreCase("2")) {
            color = Color.GREEN;
        } else if (item.MasterStatusId.equalsIgnoreCase("3")) {
            color = Color.GREEN;
        } else if (item.MasterStatusId.equalsIgnoreCase("4")) {
            color = Color.GREEN;
        } else if (item.MasterStatusId.equalsIgnoreCase("5")) {
            color = Color.CYAN;
        } else if (item.MasterStatusId.equalsIgnoreCase("6")) {
            color = Color.DKGRAY;
        } else if (item.MasterStatusId.equalsIgnoreCase("7")) {
            color = Color.RED;
        } else if (item.MasterStatusId.equalsIgnoreCase("8")) {
            color = Color.RED;
        } else if (item.MasterStatusId.equalsIgnoreCase("9")) {
            color = Color.RED;
        }
        cardView.setCardBackgroundColor(color);
        status.setText(item.status);
        tableNo.setText("Dining Park( "+item.ResRoomName+") \n Table No: "+item.tableNo);

        return v;
    }

    private class Item {

        final String tableNo;
        final String status;
        final String IsOccupied ;
        final String MasterStatusId;
        final String MasterStatusName;
        final String OfflineOrderMasterId ;
        final String ResRoomName;
        final int tableId;

        Item(String tableNo, String status, String isOccupied, String masterStatusId, String masterStatusName, String offlineOrderMasterId,String resRoomName, int tableId) {
            this.tableNo = tableNo;
            this.status = status;
            this.IsOccupied = isOccupied;
            this.MasterStatusId = masterStatusId;
            this.MasterStatusName = masterStatusName;
            this.OfflineOrderMasterId = offlineOrderMasterId;
            this.ResRoomName = resRoomName;
            this.tableId = tableId;
        }
    }

    private class WaiterTableJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            url =  URL.ServerUrl+URL.TableStatus;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            // Getting JSON from URL
            org.json.JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, token);

            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                items.clear();

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String IsOccupied = jsonObject.optString("IsOccupied");
                    String MasterStatusId = jsonObject.optString("MasterStatusId");
                    String MasterStatusName = jsonObject.optString("MasterStatusName");
                    String OfflineOrderMasterId = jsonObject.optString("OrderMasterId");
                    String MenuTitleId = jsonObject.optString("MenuTitleId");
                    String ResRoomName = jsonObject.optString("ResRoomName");
                    String ResTableId = jsonObject.optString("ResTableId");
                    String ResTableName = jsonObject.optString("ResTableName");

                    if(MasterStatusName.isEmpty() || MasterStatusName.equalsIgnoreCase("null")){
                        MasterStatusName="Empty";
                    }

                    addTable(ResTableName,MasterStatusName,ResTableId,IsOccupied,MasterStatusId,MasterStatusName,OfflineOrderMasterId,ResRoomName);

                }
                notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

}