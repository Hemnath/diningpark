package np.com.diningpark.fragment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.R;


public class ImageAdapter extends BaseAdapter {
    private Context mContext;
    private List<Item> items = new ArrayList<Item>();
    private LayoutInflater inflater;

    public ImageAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mContext = context;
    }

    public boolean addItem(Context context, int menuId, String menuTitle, ArrayList<String> foodType, String menuIngredient, String menuSuggestion, String imageURL, String rate, String calories, String menuDetail, String menuTypeId) {

        inflater = LayoutInflater.from(context);

        String imageURLFull = imageURL;
        items.add(new Item(menuId, menuTitle, imageURLFull, rate, calories, menuDetail, foodType, menuTypeId, menuIngredient, menuSuggestion));

        return true;

    }

    public boolean clearItem() {
        items.clear();

        return true;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return items.get(i).menuId;
    }

    public int getMenuId(int i) {
        return items.get(i).menuId;
    }

    public String getImageURL(int i) {
        return items.get(i).drawableId;
    }

    public String getItemName(int i) {
        return items.get(i).itemName;
    }

    public String getItemPrice(int i) {
        return items.get(i).itemPrice;
    }

    public String getItemCalories(int i) {
        return items.get(i).itemCalories;
    }

    public String getItemmenuIngredient(int i) {
        return items.get(i).menuIngredient;
    }

    public String getItemmenuSuggestion(int i) {
        return items.get(i).menuSuggestion;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;
        ImageView picture;
        TextView name, price, calories;
        ImageView foodCategory1, foodCategory2, foodCategory3, foodCategory4;
        GridView foodTypeList;

        if (v == null) {
            v = inflater.inflate(R.layout.item_list_layout, viewGroup, false);
            v.setTag(R.id.item_id, v.findViewById(R.id.item_id));
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
            v.setTag(R.id.itemName, v.findViewById(R.id.itemName));
            v.setTag(R.id.itemPrice, v.findViewById(R.id.itemPrice));
            v.setTag(R.id.itemCalories, v.findViewById(R.id.itemCalories));
            v.setTag(R.id.foodCategory1, v.findViewById(R.id.foodCategory1));
            v.setTag(R.id.foodCategory2, v.findViewById(R.id.foodCategory2));
            v.setTag(R.id.foodCategory3, v.findViewById(R.id.foodCategory3));
            v.setTag(R.id.foodCategory4, v.findViewById(R.id.foodCategory4));
        }
        picture = (ImageView) v.getTag(R.id.picture);
        name = (TextView) v.getTag(R.id.itemName);
        price = (TextView) v.getTag(R.id.itemPrice);
        calories = (TextView) v.getTag(R.id.itemCalories);
        foodCategory1 = (ImageView) v.getTag(R.id.foodCategory1);
        foodCategory2 = (ImageView) v.getTag(R.id.foodCategory2);
        foodCategory3 = (ImageView) v.getTag(R.id.foodCategory3);
        foodCategory4 = (ImageView) v.getTag(R.id.foodCategory4);

        Item item = (Item) getItem(i);

        String imagePathCon=item.drawableId;
        imagePathCon=imagePathCon.replaceAll(" ", "%20");
        com.squareup.picasso.Picasso.with(mContext).load(imagePathCon).into(picture);
        name.setText(item.itemName);
        price.setText(item.itemPrice);
        calories.setText(item.itemCalories);

        foodCategory1.setImageResource(0);
        foodCategory2.setImageResource(0);
        foodCategory3.setImageResource(0);
        foodCategory4.setImageResource(0);

        for (int f = 0; f < item.foodType.size(); f++) {
            String ft = item.foodType.get(f);
            int footType=Integer.parseInt(ft);
            int resource = 0;
            switch (footType){
                case 1:
                    resource = R.drawable.healthey;
                    break;
                case 2:
                    resource = R.drawable.delicious;
                    break;
                case 3:
                    resource = R.drawable.spicy_red;
                    break;
                case 4:
                    resource = R.drawable.veggie;
                    break;
                case 5:
                    resource = R.drawable.non_veggie;
                    break;
                default:
                    resource=0;

            }

            if (f == 0) foodCategory1.setImageResource(resource);
            else if (f == 1) foodCategory2.setImageResource(resource);
            else if (f == 2) foodCategory3.setImageResource(resource);
            else if (f == 3) foodCategory4.setImageResource(resource);
        }

        return v;
    }

    private class Item {
        final String itemName, itemPrice, drawableId, itemCalories, menuDetail, menuTypeId, menuIngredient, menuSuggestion;
        ArrayList<String> foodType;
        final int menuId;

        Item(int id, String name, String drawableId, String price, String calories, String menuDetail, ArrayList<String> foodType, String menuTypeId, String menuIngredient, String menuSuggestion) {
            this.menuId = id;
            this.menuDetail = menuDetail;
            this.menuTypeId = menuTypeId;
            this.itemName = name;
            this.drawableId = drawableId;
            this.itemPrice = price;
            this.itemCalories = calories;
            this.menuIngredient = menuIngredient;
            this.menuSuggestion = menuSuggestion;
            this.foodType = foodType;
        }
    }
}
