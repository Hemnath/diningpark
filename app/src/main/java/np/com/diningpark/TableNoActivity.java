package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import np.com.diningpark.helpers.API_Setting;

public class TableNoActivity extends AppCompatActivity {

    TextView txtTableNo;
    Button btnOK;

    public static final String DeviceId = "DeviceId";
    public static final String TableNo = "TableNo";

    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    Intent myIntent;
    private int tableId;
    private String strTableNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_no);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        tableId = sharedpreferences.getInt("tableId", 1);
        strTableNo = sharedpreferences.getString(TableNo, null);

        txtTableNo=(TextView)findViewById(R.id.tableNo);
        txtTableNo.setText("table No :- "+strTableNo);
        btnOK=(Button)findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myIntent = new Intent(TableNoActivity.this, CustomerWelcome.class);
                startActivity(myIntent);
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
