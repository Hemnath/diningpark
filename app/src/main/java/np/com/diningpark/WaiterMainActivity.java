package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import np.com.diningpark.fragment.WaiterMsgAdapter;
import np.com.diningpark.fragment.WaiterTableAdapter;
import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.AdoutusActivity;
import np.com.diningpark.helpers.DBHelper;
import np.com.diningpark.helpers.JSONParserCustom;

public class WaiterMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    GridView tableGridview;
    WaiterTableAdapter waiterTableAdapter;

    public static final String DeviceId = "DeviceId";
    SharedPreferences.Editor editor;
    Intent myIntent;
    SharedPreferences sharedpreferences;
    DBHelper mydb;

    private Timer autoUpdate;
    private API_Setting URL;
    private String strdeviceId, Token;
    private int tableId;
    private WaiterMsgAdapter waiterMsgAdapter;
    private String url;
    ArrayList<TableItem> tableItems = new ArrayList<TableItem>();

    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waiter_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);

        mydb = new DBHelper(this);

        sharedpreferences = getSharedPreferences(WelcomeActivity.MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        URL = new API_Setting();

        strdeviceId = sharedpreferences.getString(DeviceId, null);
        Token = sharedpreferences.getString("TokenId", null);
        tableId = sharedpreferences.getInt("tableId", 1);

        TextView textWaiterName = (TextView) header.findViewById(R.id.textWaiterName);
        textWaiterName.setText(sharedpreferences.getString("EmployeeName", "").replace(".", " "));

        waiterMsgAdapter = new WaiterMsgAdapter(this);

        waiterTableAdapter = new WaiterTableAdapter(this);
        tableGridview = (GridView) findViewById(R.id.tableGridView);
        tableGridview.setAdapter(waiterTableAdapter);

        tableGridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mydb.deleteAllOrderList();
                String orderMasterId = waiterTableAdapter.getOfflineOrderMasterId(position);
                String tableName = waiterTableAdapter.getTableNo(position);
                int tableId = waiterTableAdapter.getTableId(position);
                myIntent = new Intent(WaiterMainActivity.this, TableDetailActivity.class);
                myIntent.putExtra("orderMasterId", orderMasterId);
                myIntent.putExtra("tableName", tableName);
                myIntent.putExtra("tableId", Integer.toString(tableId));
                myIntent.putExtra("orderCall", "yes");

                if (autoUpdate != null) {
                    autoUpdate.cancel();
                    autoUpdate.purge();
                    autoUpdate = null;
                }
                startActivity(myIntent);
            }
        });

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (autoUpdate != null) {
            autoUpdate.cancel();
            autoUpdate.purge();
            autoUpdate = null;
        }
        autoUpdate = new Timer();
        autoUpdate.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    public void run() {
                        if (isConnected()) {
                            new MSGJSONParse().execute();
                            waiterTableAdapter.checkTableStatus();
                            waiterTableAdapter.notifyDataSetChanged();
                        }

                    }
                });
            }
        }, 0, 10000);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {

        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_helping_hand) {
            if (isConnected()) {
                new HelpingHandsAvailableTable().execute();
            } else {
                showAlertDialog(WaiterMainActivity.this, "No Internet Connection",
                        "You don't have internet connection.", false);
            }
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            myIntent = new Intent(WaiterMainActivity.this, ImageSlideShow.class);
            startActivity(myIntent);

        } else if (id == R.id.nav_msg) {
            myIntent = new Intent(WaiterMainActivity.this, WaiterMessageActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.nav_logout) {
            System.gc();
            if (autoUpdate != null) {
                autoUpdate.cancel();
                autoUpdate.purge();
                autoUpdate = null;
            }
            editor.clear();
            editor.commit();
            mydb.deleteAllOrderList();
            mydb.deleteAllOrderDetails();
            myIntent = new Intent(WaiterMainActivity.this, WelcomeActivity.class);
            this.finish();
            startActivity(myIntent);

        } else if (id == R.id.nav_reset) {
            System.gc();
            if (autoUpdate != null) {
                autoUpdate.cancel();
                autoUpdate.purge();
                autoUpdate = null;
            }
            editor.clear();
            editor.commit();
            mydb.deleteAllOrderList();
            mydb.deleteAllOrderDetails();
            myIntent = new Intent(WaiterMainActivity.this, WelcomeActivity.class);
            finish();
            startActivity(myIntent);
        } else if (id == R.id.nav_about) {
            myIntent = new Intent(WaiterMainActivity.this, AdoutusActivity.class);
            startActivity(myIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void pushNotification(String mId, String Message) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.logo)
                        .setContentTitle("Dining Park Notification")
                        .setContentText(Message);
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        mBuilder.setSound(alarmSound);
        Intent resultIntent = new Intent(this, WaiterMessageActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(WaiterMessageActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(Integer.parseInt(mId), mBuilder.build());
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("WaiterMain Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
    //todo: Menu Item JSON.

    private class MSGJSONParse extends AsyncTask<String, String, JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.MyMessage;

            JSONParserCustom jParser = new JSONParserCustom();
            JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            System.out.println("Menu list == " + json);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            String data = "";
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String InvoiceNo = jsonObject.optString("InvoiceNo");
                    String Message = jsonObject.optString("Message");
                    String OfflineMessageId = jsonObject.optString("OfflineMessageId");
                    String OrderMasterId = jsonObject.optString("OrderMasterId");
                    String SentTime = jsonObject.optString("SentTime");
                    String TableNumber = jsonObject.optString("TableNumber");


                    if (mydb.isNewMassegeId(OfflineMessageId)) {
                        pushNotification(OfflineMessageId, Message);
                        mydb.insertMassegeDetails(OfflineMessageId, Message, InvoiceNo, TableNumber, OrderMasterId, 0);
                    } else {
                        mydb.updateMassegeDetails(OfflineMessageId, 1);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private class TableStatusJSONParse extends AsyncTask<String, String, JSONObject> {

        private final String strTableNo;

        TableStatusJSONParse(String tableNo) {
            strTableNo = tableNo;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            url = URL.ServerUrl + URL.GetTableStatus + strTableNo;

            JSONParserCustom jParser = new JSONParserCustom();
            JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String MasterStatusName = jsonObject.optString("MasterStatusName");
                    String OrderMasterId = jsonObject.optString("OrderMasterId");
                    String ResTableId = jsonObject.optString("ResTableId");
                    String ResTableName = jsonObject.optString("ResTableName");

                    mydb.deleteAllOrderList();
                    myIntent = new Intent(WaiterMainActivity.this, TableDetailActivity.class);
                    myIntent.putExtra("orderMasterId", OrderMasterId);
                    myIntent.putExtra("tableName", ResTableName);
                    myIntent.putExtra("tableId", ResTableId);
                    myIntent.putExtra("orderCall", "yes");

                    if (autoUpdate != null) {
                        autoUpdate.cancel();
                        autoUpdate.purge();
                        autoUpdate = null;
                    }

                    if (isConnected()) {
                        new SENDMASSAGEJSONParse(ResTableId, ResTableName).execute();
                    } else {
                        showAlertDialog(WaiterMainActivity.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }

                    startActivity(myIntent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private class SENDMASSAGEJSONParse extends AsyncTask<Void, Void, String> {
        private final String strTableNo;
        private final String strTableName;

        SENDMASSAGEJSONParse(String tableNo, String tableName) {
            strTableNo = tableNo;
            strTableName = tableName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(Void... params) {

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.SaveNewMessage;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                JSONObject jsonObject = new JSONObject();

                String msg = "Helping Hands by " + sharedpreferences.getString("userName", "") + " To Table " + strTableName;
                jsonObject.accumulate("CustomerMessage", msg);
                jsonObject.accumulate("ResTableId", strTableNo);


                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);

                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");
                httpPost.setHeader("Token", Token);
                httpPost.setHeader("DeviceId", strdeviceId);

                HttpResponse httpResponse = httpclient.execute(httpPost);

                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                JSONObject json = new JSONObject(result);

                String Message = json.optString("Message");
                String MsgType = json.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {
                    Toast.makeText(getApplicationContext(), "Message Send " + Message,
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), Message,
                            Toast.LENGTH_SHORT).show();
                }
            } catch (
                    JSONException e
                    ) {
                e.printStackTrace();
            }
        }
    }
    private class HelpingHandsAvailableTable extends AsyncTask<String, String, JSONObject> {

        ProgressDialog asyncDialog = new ProgressDialog(WaiterMainActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            asyncDialog.setMessage("Loading...");
            asyncDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {

            url = URL.ServerUrl + URL.HelpingHandsAvailableTable;

            JSONParserCustom jParser = new JSONParserCustom();
            JSONObject json = jParser.getJSONFromUrl(url, strdeviceId, Token);

            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            ArrayList tableNums = new ArrayList();
            try {
                JSONArray jsonArray = json.optJSONArray("List");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String OrderMasterId = jsonObject.optString("OrderMasterId");
                    String ResTableId = jsonObject.optString("ResTableId");
                    String TableNumber = jsonObject.optString("ResTableName");

                    tableItems.add(new TableItem(i, OrderMasterId, ResTableId, TableNumber));
                    tableNums.add(TableNumber);

                }
                asyncDialog.cancel();
                String jsonStatus = json.optString("ActionStatus");
                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success") && MsgType.equalsIgnoreCase("Success")) {

                    final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(WaiterMainActivity.this);
                    LayoutInflater inflater = getLayoutInflater();
                    final View dialogView = inflater.inflate(R.layout.helpinghand_popup, null);
                    final Spinner helpSpin = (Spinner) dialogView.findViewById(R.id.help_spinner);

                    dialogBuilder.setView(dialogView);

                    helpSpin.setAdapter(new MySpinnerAdapter(WaiterMainActivity.this, R.layout.spinner_row, tableNums));

                    dialogBuilder.setTitle("Helping Hands");
                    dialogBuilder.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                        }
                    }).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            int pos = helpSpin.getSelectedItemPosition();
                            TableItem item = tableItems.get(pos);
                            mydb.deleteAllOrderList();
                            myIntent = new Intent(WaiterMainActivity.this, TableDetailActivity.class);
                            myIntent.putExtra("orderMasterId", item.OrderMasterId);
                            myIntent.putExtra("tableName", item.ResTableName);
                            myIntent.putExtra("tableId", item.ResTableId);
                            myIntent.putExtra("orderCall", "yes");

                            System.out.println(" Result of JSON orderMasterId : " +  item.OrderMasterId);
                            System.out.println(" Result of JSON tableName : " + item.ResTableName);
                            System.out.println(" Result of JSON tableId : " + item.ResTableId);
                            startActivity(myIntent);

                            API_Setting.helpingHand=true;

                            if (isConnected()) {
                                new SENDMASSAGEJSONParse(item.ResTableId, item.ResTableName).execute();
                            } else {
                                showAlertDialog(WaiterMainActivity.this, "No Internet Connection",
                                        "You don't have internet connection.", false);
                            }

                        }
                    });
                    AlertDialog b = dialogBuilder.create();
                    b.show();

                } else {
                    Toast.makeText(getApplicationContext(), "No Available Table Found!",
                            Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    class TableItem {
        Integer sn;
        String OrderMasterId;
        String ResTableName;
        String ResTableId;

        public TableItem(Integer sn, String orderMasterId, String resTableId , String  resTableName) {
            this.sn = sn;
            OrderMasterId = orderMasterId;
            ResTableName = resTableName;
            ResTableId = resTableId;
        }
    }

    public class MySpinnerAdapter extends ArrayAdapter<String> {
        ArrayList objects = new ArrayList();

        public MySpinnerAdapter(Context context, int textViewResourceId,
                                ArrayList objects) {
            super(context, textViewResourceId, objects);
            this.objects = objects;
        }

        @Override
        public View getDropDownView(int position, View convertView,
                                    ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater = getLayoutInflater();
            View row = inflater.inflate(R.layout.spinner_row, parent, false);
            TextView label = (TextView) row.findViewById(R.id.weekofday);
            label.setText(objects.get(position).toString());
            return row;
        }
    }


    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }
}
