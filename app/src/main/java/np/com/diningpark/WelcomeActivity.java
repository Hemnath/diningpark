package np.com.diningpark;

/**
 * Created by hemnath on 3/2/2016.
 */

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import np.com.diningpark.helpers.API_Setting;
import np.com.diningpark.helpers.JSONParserCustom;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class WelcomeActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final int REQUEST_READ_CONTACTS = 0;
    private UserLoginTask mAuthTask = null;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView, mtableNo;
    private RadioGroup mlogInAs;
    private View mLoginFormView;

    ProgressDialog progressBar;
    SharedPreferences.Editor editor;
    SharedPreferences sharedpreferences;

    public static final String UserType = "userTypeKey";
    public static final String UserName = "userName";
    public static final String EmployeeName = "EmployeeName";
    public static final String TableNo = "TableNo";
    public static final String password = "password";
    public static final String DeviceId = "DeviceId";
    public static final String BrandName = "Brand Name";
    public static final String CopyRightText = "CopyRightText";
    public static final String Email = "Email";
    public static final String PhoneNo = "PhoneNo";
    public static final String ResAddress = "ResAddress";
    public static final String RestaurentName = "RestaurentName";
    public static final String ServiceChargeRate = "ServiceChargeRate";
    public static final String VatRate = "VatRate";
    public static final String MyPREFERENCES = "MyPrefs";
    String tokenId;

    private String android_id = "";
    private String loginUserType = "";

    Intent myIntent;
    String type;
    API_Setting URL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        populateAutoComplete();
        mlogInAs = (RadioGroup) findViewById(R.id.loginType);
        mtableNo = (EditText) findViewById(R.id.tableNo);
        mPasswordView = (EditText) findViewById(R.id.password);
        mLoginFormView = findViewById(R.id.login_form);

        android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),Settings.Secure.ANDROID_ID);

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        URL = new API_Setting();

        if (sharedpreferences.contains(UserType)) {
            type = sharedpreferences.getString(UserType, null);
            if (type.equalsIgnoreCase("waiter")) {
                mEmailView.setText(sharedpreferences.getString(UserName, null));
                mPasswordView.setText(sharedpreferences.getString(password, null));
                mtableNo.setText(sharedpreferences.getString(TableNo, null));
                mlogInAs.check(R.id.loginWaiter);
                loginUserType = "waiter";

                if (isConnected()) {
                    attemptLogin();
                } else {
                    showAlertDialog(WelcomeActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }

            } else if (type.equalsIgnoreCase("Table")) {
                mEmailView.setText(sharedpreferences.getString(UserName, null));
                mPasswordView.setText(sharedpreferences.getString(password, null));
                mtableNo.setText(sharedpreferences.getString(TableNo, null));
                mlogInAs.check(R.id.loginTable);
                loginUserType = "Table";
                if (isConnected()) {
                    attemptLogin();
                } else {
                    showAlertDialog(WelcomeActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
            }
        }

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    if (isConnected()) {
                        attemptLogin();
                    } else {
                        showAlertDialog(WelcomeActivity.this, "No Internet Connection",
                                "You don't have internet connection.", false);
                    }
                    return true;
                }
                return false;
            }
        });

        mlogInAs.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rbButton = (RadioButton) findViewById(checkedId);
                TextInputLayout txtTableNum = (TextInputLayout) findViewById(R.id.txtTableNum);
                if (rbButton.isChecked()) {
                    loginUserType = rbButton.getText().toString();
                    if (loginUserType.equalsIgnoreCase("table")) {
                        //txtTableNum.setVisibility(View.VISIBLE);
                        //System.out.println(loginUserType);
                    } else {
                        //txtTableNum.setVisibility(View.GONE);
                    }
                }
            }
        });
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConnected()) {
                    attemptLogin();
                } else {
                    showAlertDialog(WelcomeActivity.this, "No Internet Connection",
                            "You don't have internet connection.", false);
                }
            }
        });
    }

    private void populateAutoComplete() {
        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }



    @Override
    public void onBackPressed() {
        myIntent = new Intent(WelcomeActivity.this, LoadingActivity.class);
        startActivity(myIntent);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    boolean isConnected() {
        ConnectivityManager cm = (ConnectivityManager) this.getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        return isConnected;
    }

    public void showAlertDialog(Context context, String title, String message, Boolean status) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setIcon((status) ? R.drawable.error : R.drawable.error);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        alertDialog.show();
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
            Snackbar.make(mEmailView, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        @TargetApi(Build.VERSION_CODES.M)
                        public void onClick(View v) {
                            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                        }
                    });
        } else {
            requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }

    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();
        String tableNo = mtableNo.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {

            focusView.requestFocus();
        } else {
            progressBar = new ProgressDialog(WelcomeActivity.this);
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setMessage("Authenticating...");
            progressBar.setProgress(0);
            progressBar.show();

            mAuthTask = new UserLoginTask(email, password, tableNo, android_id, loginUserType);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(WelcomeActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;
        private final String mPassword;
        private final String mTableNumber;
        private final String mDeviceId;
        private final String mLoginType;

        UserLoginTask(String email, String password, String tableNumber, String deviceId, String loginAs) {
            mEmail = email;
            mPassword = password;
            mTableNumber = tableNumber;
            mDeviceId = deviceId;
            mLoginType = loginAs;
        }

        @Override
        protected String doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

            InputStream inputStream = null;
            String result = "";

            String url = URL.ServerUrl + URL.UserLogin;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(url);
                String json = "";
                // 3. build jsonObject
                JSONObject jsonObject = new JSONObject();

                jsonObject.accumulate("UserName", mEmail);
                jsonObject.accumulate("Password", mPassword);
                jsonObject.accumulate("TableNumber", mTableNumber);
                jsonObject.accumulate("DeviceId", mDeviceId);
                jsonObject.accumulate("LoginType", mLoginType);

                json = jsonObject.toString();
                StringEntity se = new StringEntity(json);

                httpPost.setEntity(se);
                httpPost.setHeader("Accept", "application/json");
                httpPost.setHeader("Content-type", "application/json");

                HttpResponse httpResponse = httpclient.execute(httpPost);
                inputStream = httpResponse.getEntity().getContent();

                if (inputStream != null)
                    result = convertInputStreamToString(inputStream);

                return result;

            } catch (Exception e) {
                System.out.println(" Result of JSON : " + e);
            }

            return result;
        }

        @Override
        protected void onPostExecute(final String result) {
            boolean success = false;
            mAuthTask = null;
            try {
                JSONObject json = new JSONObject(result);
                String jsonStatus = json.optString("ActionStatus");

                JSONObject jsonObject = new JSONObject(jsonStatus);

                String Message = jsonObject.optString("Message");
                String MsgType = jsonObject.optString("MsgType");

                if (Message.equalsIgnoreCase("Success.") && MsgType.equalsIgnoreCase("Success")) {
                    success = true;
                }

                if (success) {
                    tokenId = json.optString("Token");
                    int tableId = Integer.parseInt(json.optString("TableId"));
                    String strEmployeeName = json.optString("EmployeeName");
                    editor.putString(EmployeeName, strEmployeeName);
                    editor.putString("TokenId", tokenId);
                    editor.putString(UserName, mEmail);
                    editor.putString(password, mPassword);
                    editor.putString(DeviceId, mDeviceId);
                    editor.putString(UserType, mLoginType);
                    editor.putString(TableNo, mTableNumber);
                    editor.putInt("tableId", tableId);
                    editor.commit();
                    new appSettingJSONParse().execute();
                } else {
                    //showProgress(false);
                    if (progressBar.isShowing()) {
                        progressBar.dismiss();
                    }
                    mPasswordView.setError(getString(R.string.error_incorrect_password));
                    mPasswordView.requestFocus();
                    Toast.makeText(WelcomeActivity.this, Message,
                            Toast.LENGTH_SHORT).show();
                }
                //output.setText(data);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            //showProgress(false);
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
        }
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }
        reader.close();
        return out.toString();
    }

    private class appSettingJSONParse extends android.os.AsyncTask<String, String, org.json.JSONObject> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected org.json.JSONObject doInBackground(String... args) {
            // np.com.diningpark.helpers.API_Setting url = new np.com.diningpark.helpers.API_Setting();
            String url = URL.ServerUrl + URL.AppSetting;

            JSONParserCustom jParser = new np.com.diningpark.helpers.JSONParserCustom();
            // Getting JSON from URL

            org.json.JSONObject json = jParser.getJSONFromUrl(url, android_id, tokenId);
            System.out.println("json value: " + json);
            return json;
        }

        @Override
        protected void onPostExecute(org.json.JSONObject json) {
            //showProgress(false);
            String strBrandName = json.optString("BrandName");
            String strCopyRightText = json.optString("CopyRightText");
            String strEmail = json.optString("MenuDetail");
            String strPhoneNo = json.optString("PhoneNo");
            String strResAddress = json.optString("ResAddress");
            String strRestaurentName = json.optString("RestaurentName");
            String strServiceChargeRate = json.optString("ServiceChargeRate");
            String strVarRate = json.optString("VarRate");

            editor.putString(BrandName, strBrandName);
            editor.putString(CopyRightText, strCopyRightText);
            editor.putString(Email, strEmail);
            editor.putString(PhoneNo, strPhoneNo);
            editor.putString(ResAddress, strResAddress);
            editor.putString(RestaurentName, strRestaurentName);
            editor.putString(ServiceChargeRate, strServiceChargeRate);
            editor.putString(VatRate, strVarRate);
            editor.commit();
            finish();
            if (progressBar.isShowing()) {
                progressBar.dismiss();
            }
            if (loginUserType.equalsIgnoreCase("table"))
                myIntent = new Intent(WelcomeActivity.this, TableNoActivity.class);
            else
                myIntent = new Intent(WelcomeActivity.this, WaiterMainActivity.class);
            startActivity(myIntent);
        }
    }

}

